<?php

  class ConexaoView
  {

    public function cadastra()
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar conexão</legend>
          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="conexao">Nome da Conexão*:</label>
              <input type="text" name="conexao" id="conexao" value="" required>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra

    public function lista($conexoes)
    {
      
      ?>
      <div class="container-fluid">
        <h1 align="right" class="w-25">Conexões</h1>
        <div class="w-25">
          <a href=<?php HOME_URI; ?>"index.php?page=conexao&view=cadastra" role="button" class="btn btn-success">+ Cadastrar Conexão</a>
        </div><br/>

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Nome da Conexão</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($conexoes as $c): ?>
            <tr>
              <th><a href="<?php echo HOME_URI."index.php?page=conexao&view=consulta&id={$c['id_conexao']}"; ?>"><?php echo $c['conexao']; ?></a></th>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>

      </div>

      <?php 
      
    } // fim lista

    public function consulta($conexao) {

      
      ?>
      <div class="container-fluid">

        <h3>Conexões</h3>
		
        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=conexao&view=edita&id={$conexao['id_conexao']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=conexao&view=exclui&id={$conexao['id_conexao']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
          </div>
        </div>
		
        <div class="row">
          <div class="col-md-4">
            <p><strong>Nome da conexão:</strong></p>
            <p><?php echo $conexao['conexao']; ?></p>
          </div>

        </div>

      </div>
      <?php 

    } // fim da consulta

    public function edita($c)
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar conexão</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="conexao">Nome do conexao*:</label>
              <input type="text" name="conexao" id="conexao" value="<?php echo $c['conexao']; ?>" required> <br>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php

    }//fim edita


  } // fim classe

?>  






