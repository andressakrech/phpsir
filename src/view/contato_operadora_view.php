<?php

  class ContatoOperadoraView
  {

    public function cadastra()
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar contato</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="contato_Operadora_nome">Nome*:</label>
              <input type="text" name="nome" id="nome" value="" required> <br>
            </div>

            <div class="form-group">
              <label for="email_contato">Email:</label>
              <input type="email" name="email" id="email" value="" placeholder="email@dominio.com">
            </div>

            <div class="form-group">
              <label for="telefone_contato">Telefone:</label>
              <input type="text" name="telefone" id="telefone" value=""> <br>
            </div>

            <div class="form-group">
              <label for="observacao">Observação:</label>
              <textarea cols="30" type="text" name="observacao" id="observacao"></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra

    public function lista($contatos, $idOperadora) {

    ?>

      <div class="container-fluid">

        <h3>Contatos:</h3>

        <div class="w-25"><a href=<?php HOME_URI; ?>"index.php?page=contato_operadora&view=cadastra&operadora=<?php echo $idOperadora?>" role="button" class="btn btn-success">+ Cadastrar contato</a>
        </div><br/>

        <div>
          <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Email</th>
              <th scope="col">Telefone</th>
              <th scope="col">Observação</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($contatos as $c): ?>
            <tr>
              <td><a href="<?php echo HOME_URI."index.php?page=contato_operadora&view=consulta&id={$c['id_contato_operadora']}"; ?>"><?php echo $c['nome']; ?></a></td>
              <td><?php echo $c['telefone']; ?></td>
              <td><?php echo $c['email']; ?></td>
              <td><?php echo $c['observacao']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        </div>
		
      </div><!-- fim container-->

    <?php

    }

    public function consulta($contatoOperadora) {

      ?>
      <div class="container-fluid">

        <h3>Contato Operadora</h3>
		
        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=contato_operadora&view=edita&id={$contatoOperadora['id_contato_operadora']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=contato_operadora&view=exclui&id={$contatoOperadora['id_contato_operadora']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
          </div>
        </div>
		
        <div class="row">

          <div class="col-md-4">
            <p><strong>Nome:</strong></p>
            <p><?php echo $contatoOperadora['nome']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Observação:</strong></p>
            <textarea cols="30" readonly><?php echo $contatoOperadora['observacao']; ?></textarea>
          </div>

          <div class="col-md-4">
            <p><strong>Email:</strong></p>
            <p><?php echo $contatoOperadora['email']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Telefone:</strong></p>
            <p><?php echo $contatoOperadora['telefone']; ?></p>
          </div>



        </div><!-- Fim class row -->

      </div>
      <?php 

    } // fim da consulta

    public function edita($e)
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar contato da operadora</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="contato_operadora_nome">Nome do contato*:</label>
              <input type="text" name="nome" id="nome" value="<?php echo $e['nome']; ?>" required> <br>
            </div>

            <div class="form-group">
              <label for="email_contato">Email:</label>
              <input type="email" name="email" id="email" value="<?php echo $e['email']; ?>">
            </div>

            <div class="form-group">
              <label for="telefone_contato">Telefone:</label>
              <input type="text" name="telefone" id="telefone" value="<?php echo $e['telefone']; ?>"> <br>
            </div>

            <div class="form-group">
              <label for="observacao">Observação:</label>
              <textarea cols="30" type="text" name="observacao" id="observacao"><?php echo $e['observacao']; ?></textarea>
            </div>

            <input type="hidden" name="operadora" id="operadora" value="<?php echo $e['id_operadora']; ?>"> <br>


            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra


  } // fim classe

?>  

