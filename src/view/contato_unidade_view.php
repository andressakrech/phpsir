<?php

  class ContatoUnidadeView
  {

    public function cadastra()
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar contato</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="contato_Unidade_nome">Nome*:</label>
              <input type="text" name="nome" id="nome" value="" required> <br>
            </div>

            <div class="form-group">
              <label for="email_contato">Email:</label>
              <input type="email" name="email" id="email" value="" placeholder="email@dominio.com">
            </div>

            <div class="form-group">
              <label for="telefone_contato">Telefone:</label>
              <input type="text" name="telefone" id="telefone" value=""> <br>
            </div>

            <div class="form-group">
              <label for="observacao">Observação:</label>
              <textarea cols="30" type="text" name="observacao" id="observacao" value=""></textarea>
            </div>

            <div class="form-group">
                <label class="checkbox-inline"><input type="checkbox" name="tecnico">Tecnico</label>
                <label class="checkbox-inline"><input type="checkbox" name="seguranca">Seguranca</label>
                <label class="checkbox-inline"><input type="checkbox" name="administrativo">Administrativo</label>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra

    public function consulta($contatoUnidade) {

      // #TODO recebe contatoUnidade, mostra e apresenta opções para editar ou excluir
      
      ?>
      <div class="container-fluid">

        <h3>Contato Unidade</h3>
		
        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=contato_unidade&view=edita&id={$contatoUnidade['id_contato_unidade']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=contato_unidade&view=exclui&id={$contatoUnidade['id_contato_unidade']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
          </div>
        </div>
		
        <div class="row">

          <div class="col-md-4">
            <p><strong>Nome:</strong></p>
            <p><?php echo $contatoUnidade['nome']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Observação:</strong></p>
            <textarea cols="30" readonly><?php echo $contatoUnidade['observacao']; ?></textarea>
          </div>

          <div class="form-group">
            <p><strong>Enquadramentos:</strong></p>
            <label class="checkbox-inline"><input type="checkbox" name="tecnico" readonly="true" 
            <?php echo ($contatoUnidade['tecnico'] ? "checked" : ""); ?>>Tec</label>
            <label class="checkbox-inline"><input type="checkbox" name="seguranca" readonly="true" 
            <?php echo ($contatoUnidade['seguranca']) ? "checked" : ""; ?>>Seg</label>
            <label class="checkbox-inline"><input type="checkbox" name="administrativo" readonly="true"
            <?php echo ($contatoUnidade['administrativo']) ? "checked" : ""; ?>>Adm</label>
          </div>

          <div class="col-md-4">
            <p><strong>Email:</strong></p>
            <p><?php echo $contatoUnidade['email']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Telefone:</strong></p>
            <p><?php echo $contatoUnidade['telefone']; ?></p>
          </div>



        </div><!-- Fim class row -->

      </div>
      <?php 

    } // fim da consulta

    public function lista($contatos, $idUnidade) {

    ?>
      <div class="container-fluid">

        <h3>Contatos:</h3>

        <div class="w-25"><a href=<?php HOME_URI; ?>"index.php?page=contato_unidade&view=cadastra&unidade=<?php echo $idUnidade;?>" role="button" class="btn btn-success">+ Cadastrar contato</a>
        </div><br/>

        <div>
          <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Email</th>
              <th scope="col">Telefone</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($contatos as $c): ?>
            <tr>
              <td><a href="<?php echo HOME_URI."index.php?page=contato_unidade&view=consulta&id={$c['id_contato_unidade']}"; ?>"><?php echo $c['nome']; ?></a></td>
              <td><?php echo $c['telefone']; ?></td>
              <td><?php echo $c['email']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        </div>

      </div><!-- fim container-->
      <?php

    }


    public function edita($e)
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar contatoUnidade</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="contato_unidade_nome">Nome do contato*:</label>
              <input type="text" name="nome" id="nome" value="<?php echo $e['nome']; ?>" required> <br>
            </div>

            <div class="form-group">
              <label for="observacao">Observação:</label>
              <textarea cols="30" type="text" name="observacao" id="observacao" value="<?php echo $e['observacao']; ?>"></textarea>
            </div>

            <div class="form-group">
                <label class="checkbox-inline"><input type="checkbox" name="tecnico"
                <?php echo ($e['tecnico']) ? "checked" : ""; ?>>Tecnico</label>
                <label class="checkbox-inline"><input type="checkbox" name="seguranca"
                <?php echo ($e['seguranca']) ? "checked" : ""; ?>>Seguranca</label>
                <label class="checkbox-inline"><input type="checkbox" name="administrativo"
                <?php echo ($e['administrativo']) ? "checked" : ""; ?>>Administrativo</label>
            </div>

            <div class="form-group">
              <label for="email_contato">Email:</label>
              <input type="email" name="email" id="email" value="<?php echo $e['email']; ?>">
            </div>

            <div class="form-group">
              <label for="telefone_contato">Telefone:</label>
              <input type="text" name="telefone" id="telefone" value="<?php echo $e['telefone']; ?>"> <br>
            </div>

            <input type="hidden" name="unidade" id="unidade" value="<?php echo $e['id_unidade']; ?>"> <br>


            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra


  } // fim classe

?>  






