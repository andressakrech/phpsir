<?php

  class UnidadeView
  {

    public function cadastra($instituicoes)
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar unidade</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="unidade_nome">Nome*:</label>
              <input type="text" name="unidade_nome" id="unidade_nome" value="" required autofocus>
            </div>

            <div class="form-group">
              <label for="unidade_sigla">Sigla*:</label>
              <input type="text" name="unidade_sigla" id="unidade_sigla" value="" required>
            </div>

            <div class="form-group">
              <label for="data_conexao">Data conexão:</label>
              <input type="date" name="data_conexao" id="data_conexao" value="">
            </div>

            <div class="form-group">
              <label for="observacao">Observação:</label>
              <textarea type="text" name="observacao" id="observacao" value=""></textarea>
            </div>

            <div class="form-group">
              <label for"id_instituicao">Instituição*:</label>
              <select name="id_instituicao" required>
                <option value"">--Selecione uma opção--</option>
              <?php foreach ($instituicoes as $k=>$i): ?>
                <option value="<?php echo $k;?>"><?php echo $i['instituicao_sigla'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>
          </fieldset>
          <fieldset class="border p-2">
            <legend class="w-auto">Endereço da unidade</legend>
            <div class="form-group">
              <label for="endereco">Endereço*:</label>
              <input type="text" name="endereco" id="endereco" value="" required>
            </div>

            <div class="form-group">
              <label for="bairro">Bairro*:</label>
              <input type="text" name="bairro" id="bairro" value="">
            </div>

            <div class="form-group">
              <label for="cidade">Cidade*:</label>
              <input type="text" name="cidade" id="cidade" value="" required>
            </div>

            <div class="form-group">
              <label for="cep">Cep:</label>
              <input type="text" name="cep" id="cep" value="">
            </div>

            <div class="form-group">
              <label for="coordenada">Coordenada*:</label>
              <input type="text" name="coordenada" id="coordenada" value="" required>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra


    public function lista($unidades, $instituicoes)
    {
      
      ?>
      <div class="container-fluid">
        <h1 align="right" class="w-25">Unidades</h1>

        <div class="w-25"><a href=<?php HOME_URI; ?>"index.php?page=unidade&view=cadastra" role="button" class="btn btn-success">+ Cadastrar unidade</a>
        </div><br/>

        <div>
          <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Sigla</th>
              <th scope="col">Instituição</th>
              <th scope="col">Data de conexão</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($unidades as $k=>$u): ?>
            <tr>
              <td><a href="<?php echo HOME_URI."index.php?page=unidade&view=consulta&id={$u['id_unidade']}"; ?>"><?php echo $u['unidade_nome']; ?></a></td>
              <td><?php echo $u['unidade_sigla']; ?></td>
              <td><?php echo $instituicoes[$u['id_instituicao']]['instituicao_sigla']; ?></td>
              <td><?php echo $u['data_conexao']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        </div>

      </div><!-- fim container-->

      <?php 

    } // fim lista

    public function consulta($unidade, $instituicoes, $contatos = array()) {

      
      ?>

      <div class="container-fluid">

        <h3>Unidade</h3>

        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=unidade&view=edita&id={$unidade['id_unidade']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=unidade&view=exclui&id={$unidade['id_unidade']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <p><strong>Nome:</strong></p>
            <p><?php echo $unidade['unidade_nome']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Sigla:</strong></p>
            <p><?php echo $unidade['unidade_sigla']; ?></strong></p>
          </div>

          <div class="col-md-4">
            <p><strong>Data conexão:</strong></p>
            <p><?php echo $unidade['data_conexao']; ?></strong></p>
          </div>

          <div class="col-md-4">
            <p><strong>Observação:</strong></p>
            <p><?php echo $unidade['observacao']; ?></strong></p>
          </div>


          <div class="col-md-4">
            <p><strong>Instituição:</strong></p>
            <p><?php echo $instituicoes[$unidade['id_instituicao']]; ?></strong></p>
          </div>

        </div>
        <h3>Endereço</h3>
        <div class="row">
          <div class="col-md-4">
            <p><strong>Endereço:</strong></p>
            <p><?php echo $unidade['endereco']['endereco']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Bairro:</strong></p>
            <p><?php echo $unidade['endereco']['bairro']; ?></strong></p>
          </div>

          <div class="col-md-4">
            <p><strong>Cidade:</strong></p>
            <p><?php echo $unidade['endereco']['cidade']; ?></strong></p>
          </div>

          <div class="col-md-4">
            <p><strong>Cep:</strong></p>
            <p><?php echo $unidade['endereco']['cep']; ?></strong></p>
          </div>

          <div class="col-md-4">
            <p><strong>Coordenadas:</strong></p>
            <p><?php echo $unidade['endereco']['coordenada']; ?></strong></p>
          </div>

        </div>

        </div><!-- fecha div row -->

      </div><!-- fim container-->

      <?php 

    } // fim da consulta

    public function edita($unidade, $instituicoes)
    {

      ?>

      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar unidade</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">

            <div class="form-group">
              <label for="unidade_nome">Nome*:</label>
              <input type="text" name="unidade_nome" id="unidade_nome"
              value="<?php echo $unidade['unidade_nome'];?>" required autofocus>
            </div>

            <div class="form-group">
              <label for="unidade_sigla">Sigla*:</label>
              <input type="text" name="unidade_sigla" id="unidade_sigla"
              value="<?php echo $unidade['unidade_sigla'];?>" required>
            </div>

            <div class="form-group">
              <label for="data_conexao">Data conexão:</label>
              <input type="date" name="data_conexao" id="data_conexao"
              value="<?php echo $unidade['data_conexao'];?>">
            </div>

            <div class="form-group">
              <label for="observacao">Observação:</label>
              <textarea type="text" name="observacao" id="observacao"><?php echo $unidade['observacao'];?></textarea>
            </div>

            <div class="form-group">
              <label for"id_instituicao">Instituição*:</label>
              <select name="id_instituicao" required>
                <option value"">--Selecione uma opção--</option>
              <?php foreach ($instituicoes as $k=>$i): ?>
                <option value="<?php echo $k;?>"<?php echo ($i['id_instituicao'] == $unidade['id_instituicao']) ? " selected" : "";?>>
                  <?php echo $i['instituicao_sigla'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

          </fieldset>
          <fieldset class="border p-2">
            <legend class="w-auto">Endereço da unidade</legend>

              <input type="hidden" name="id_endereco" id="id_endereco"
              value="<?php echo $unidade['endereco']['id_endereco'];?>" required>

            <div class="form-group">
              <label for="endereco">Endereço*:</label>
              <input type="text" name="endereco" id="endereco"
              value="<?php echo $unidade['endereco']['endereco'];?>" required>
            </div>

            <div class="form-group">
              <label for="bairro">Bairro*:</label>
              <input type="text" name="bairro" id="bairro"
              value="<?php echo $unidade['endereco']['bairro'];?>">
            </div>

            <div class="form-group">
              <label for="cidade">Cidade*:</label>
              <input type="text" name="cidade" id="cidade"
              value="<?php echo $unidade['endereco']['cidade'];?>" required>
            </div>

            <div class="form-group">
              <label for="cep">Cep:</label>
              <input type="text" name="cep" id="cep"
              value="<?php echo $unidade['endereco']['cep'];?>">
            </div>

            <div class="form-group">
              <label for="coordenada">Coordenada*:</label>
              <input type="text" name="coordenada" id="coordenada"
              value="<?php echo $unidade['endereco']['coordenada'];?>" required>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>

      <?php
      


    }//fim edita


  } // fim classe

?>
