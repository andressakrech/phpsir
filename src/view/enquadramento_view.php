<?php

  class EnquadramentoView
  {

    public function cadastra()
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar enquadramento</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="enquadramento_nome">Nome do enquadramento*:</label>
              <input type="text" name="enquadramento_nome" id="enquadramento_nome" value="" required> <br>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra

    public function lista($enquadramentos)
    {
      
      ?>

      <div class="container-fluid">
        <h1 align="right" class="w-25">Enquadramentos</h1>

        <div class="w-25"><a href=<?php HOME_URI; ?>"index.php?page=enquadramento&view=cadastra" role="button" class="btn btn-success">+ Cadastrar enquadramento</a>
        </div><br/>

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Nome do enquadramento</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($enquadramentos as $k=>$e): ?>
            <tr>
              <th><a href="<?php echo HOME_URI."index.php?page=enquadramento&view=consulta&id={$e['id_enquadramento']}"; ?>"><?php echo $e['enquadramento_nome']; ?></a></th>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>

      </div>

      <?php 
      
    } // fim lista

    public function consulta($enquadramento) {

      // #TODO recebe enquadramento, mostra e apresenta opções para editar ou excluir
      
      ?>
      <div class="container-fluid">

        <h3>Enquadramento</h3>
		
        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=enquadramento&view=edita&id={$enquadramento['id_enquadramento']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=enquadramento&view=exclui&id={$enquadramento['id_enquadramento']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
          </div>
        </div>
		
        <div class="row">
          <div class="col-md-4">
            <p><strong>Nome do enquadramento:</strong></p>
            <p><?php echo $enquadramento['enquadramento_nome']; ?></p>
          </div>

        </div>

      </div>
      <?php 

    } // fim da consulta

    public function edita($e)
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar enquadramento</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="enquadramento_nome">Nome do enquadramento*:</label>
              <input type="text" name="enquadramento_nome" id="enquadramento_nome" value="<?php echo $e['enquadramento_nome']; ?>" required> <br>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra


  } // fim classe

?>  






