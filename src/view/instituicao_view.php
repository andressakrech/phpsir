<?php

  class InstituicaoView
  {

    public function cadastra($enquadramentos)
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar instituição</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="instituicao_nome">Nome*:</label>
              <input type="text" name="instituicao_nome" id="instituicao_nome" value="" required autofocus>
            </div>

            <div class="form-group">
              <label for="instituicao_sigla">Sigla*:</label>
              <input type="text" name="instituicao_sigla" id="instituicao_sigla" value="" required>
            </div>

            <div class="form-group">
              <label for="instituicao_site">Site:</label>
              <input type="url" name="instituicao_site" id="instituicao_site" value="">
            </div>

            <div class="form-group">
              <label for"id_enquadramento">Enquadramento:</label>
              <select name="id_enquadramento" required>
              <?php foreach ($enquadramentos as $e): ?>
                <option value='<?php echo $e['id_enquadramento'];?>'><?php echo $e['enquadramento_nome'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra

    public function lista($instituicoes, $enquadramentos)
    {
      
      ?>
      <div class="container-fluid">
        <h1 align="right" class="w-25">Instituições</h1>

        <div class="w-25"><a href=<?php HOME_URI; ?>"index.php?page=instituicao&view=cadastra" role="button" class="btn btn-success">+ Cadastrar instituição</a>
        </div><br/>

        <div>
          <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th scope="col">Sigla</th>
              <th scope="col">Nome</th>
              <th scope="col">site</th>
              <th scope="col">enquadramento</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($instituicoes as $k=>$i): ?>
            <tr>
              <td><a href="<?php echo HOME_URI."index.php?page=instituicao&view=consulta&id={$i['id_instituicao']}"; ?>"><?php echo $i['instituicao_sigla']; ?></a></td>
              <td><?php echo $i['instituicao_nome']; ?></td>
              <td><?php echo $i['instituicao_site']; ?></td>
              <td><?php echo $enquadramentos[$i['id_enquadramento']]; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        </div>

      </div><!-- fim container-->

      <?php 

    } // fim lista

    public function consulta($instituicao, $enquadramentos, $unidade = array()) {

      
      ?>

      <div class="container-fluid">

        <h3>Instituição</h3>
		
        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=instituicao&view=edita&id={$instituicao['id_instituicao']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=instituicao&view=exclui&id={$instituicao['id_instituicao']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
          </div>
        </div>
		
        <div class="row">
          <div class="col-md-4">
            <p><strong>Nome:</strong></p>
            <p><?php echo $instituicao['instituicao_nome']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Sigla:</strong></p>
            <p><?php echo $instituicao['instituicao_sigla']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Site:</strong></p>
            <p><?php echo $instituicao['instituicao_site']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Enquadramento:</strong></p>
            <p><?php echo $enquadramentos[$instituicao['id_enquadramento']]; ?></p>
          </div>

        </div>

      </div><!-- fim container-->

      <?php 

    } // fim da consulta

    public function edita($instituicao, $enquadramentos/*, $reitoria*/)
    {
      
      ?>

      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar instituição</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="instituicao_nome">Nome*:</label>
              <input type="text" name="instituicao_nome" id="instituicao_nome"
              value="<?php echo $instituicao['instituicao_nome'];?>" required autofocus>
            </div>

            <div class="form-group">
              <label for="instituicao_sigla">Sigla*:</label>
              <input type="text" name="instituicao_sigla" id="instituicao_sigla"
              value="<?php echo $instituicao['instituicao_sigla'];?>" required>
            </div>

            <div class="form-group">
              <label for="instituicao_site">Site:</label>
              <input type="url" name="instituicao_site" id="instituicao_site"
              value="<?php echo $instituicao['instituicao_site'];?>">
            </div>

            <div class="form-group">
              <label for"id_enquadramento">Enquadramento:</label>
              <select name="id_enquadramento" required>
              <?php foreach ($enquadramentos as $k=>$e): ?>

                <option value="<?php echo $k;?>"<?php echo ($k == $instituicao['id_enquadramento']) ? " selected": "" ?>><?php echo $e;?></option>";
              <?php endforeach; ?>
              </select>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>

      <?php
      


    }//fim cadastra


  } // fim classe

?>
