<?php
  include_once(ABSPATH.'/functions/exibe_banda.php');

  class CircuitoView
  {

    public function cadastra($operadoras, $unidades, $conexao)
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar circuito</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="designacao">Designação*:</label>
              <input type="text" name="designacao" id="designacao" value="" required autofocus>
            </div>

            <div class="form-group">
              <label for"id_unidade">Unidade:</label>
              <select name="id_unidade" required>
              <option value="">Selecione uma unidade</option>
              <?php foreach ($unidades as $u): ?>
                <option value='<?php echo $u['id_unidade'];?>'><?php echo $u['unidade_sigla'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group">
              <label for"id_unidade">Operadora:</label>
              <select name="id_operadora" required>
              <option value="">Selecione uma operadora</option>
              <?php foreach ($operadoras as $o): ?>
                <option value='<?php echo $o['id_operadora'];?>'><?php echo $o['operadora_sigla'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group">
              <label for="banda">Banda:</label>
              <input type="number" name="banda" id="banda" value="" placeholder="banda em Mbps">
            </div>

            <div class="form-group">
              <label for="data_ativacao">Data ativação:</label>
              <input type="date" name="data_ativacao" id="data_ativacao" value="">
            </div>

            <div class="form-group">
              <label for="valor_mensal">Valor Mensal:</label>
              <input type="number" name="valor_mensal" id="valor_mensal" value="">
            </div>

            <div class="form-group">
              <label for"conexao">Conexao:</label>
              <select name="conexao" required>
              <option value="">Selecione uma conexão</option>
              <?php foreach ($conexao as $c): ?>
                <option value='<?php echo $c['id_conexao'];?>'><?php echo $c['conexao'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

           <div class="form-group">
             <label for"conexao">Ativo:</label>
             <label class="radio-inline"><input type="radio" name="ativo" value="1" checked>Sim</label>
             <label class="radio-inline"><input type="radio" name="ativo" value="0">Não</label>
           </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra


    public function lista($circuitos, $operadoras, $unidades, $conexoes)
    {
      include_once(ABSPATH.'/functions/exibe_banda.php');

      ?>
      <div class="container-fluid">
        <h1 align="right" class="w-25">Circuitos</h1>

        <div class="w-25"><a href=<?php HOME_URI; ?>"index.php?page=circuito&view=cadastra" role="button" class="btn btn-success">+ Cadastrar circuito</a>
        </div><br/>

        <div>
          <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th scope="col">Designação</th>
              <th scope="col">Unidade</th>
              <th scope="col">Operadora</th>
              <th scope="col">Banda</th>
              <th scope="col">Conexão</th>
              <th scope="col">Ativo</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($circuitos as $c): ?>
            <tr>
              <td><a href="<?php echo HOME_URI."index.php?page=circuito&view=consulta&id={$c['id_circuito']}"; ?>"><?php echo $c['designacao']; ?></a></td>
              <td><?php echo $unidades[$c['id_unidade']]['unidade_nome']; ?></td>
              <td><?php echo $operadoras[$c['id_operadora']]['operadora_nome']; ?></td>
              <td><?php echo exibeBanda($c['banda']); ?></td>
              <td><?php echo $conexoes[$c['id_conexao']]['conexao']; ?></td>
              <td><?php echo ($c['ativo'] == 1) ? 'ativo' : 'inativo'; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        </div>

      </div><!-- fim container-->

      <?php 

    } // fim lista

    public function consulta($circuito, $operadoras, $unidades, $conexoes) {
      ?>

      <div class="container-fluid">

        <h3>Circuito</h3>

        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=circuito&view=edita&id={$circuito['id_circuito']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=circuito&view=exclui&id={$circuito['id_circuito']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <p><strong>Designação:</strong></p>
            <p><?php echo $circuito['designacao']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Unidade:</strong></p>
            <p><?php echo $unidades[$circuito['id_unidade']]['unidade_sigla']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Operadora:</strong></p>
            <p><?php echo $operadoras[$circuito['id_operadora']]['operadora_sigla']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Banda:</strong></p>
            <p><?php echo exibeBanda($circuito['banda']); ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Data ativação:</strong></p>
            <p><?php echo $circuito['data_ativacao']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Valor mensal:</strong></p>
            <p><?php echo $circuito['valor_mensal']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Conexão:</strong></p>
            <p><?php echo $conexoes[$circuito['id_conexao']]['conexao']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Status:</strong></p>
            <p><?php echo ($circuito['ativo'] == 1) ? "ativo" : "inativo"; ?></p>
          </div>

        </div>

      </div><!-- fim container-->

      <?php 

    } // fim da consulta

    public function edita($circuito, $operadoras, $unidades, $conexoes)
    {
      ?>

      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar circuito</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">

          <input type="hidden" name="id_circuito" id="id_circuito"
          value="<?php echo $circuito['id_circuito'];?>" required>

            <div class="form-group">
              <label for="designacao">Designação*:</label>
              <input type="text" name="designacao" id="designacao" value="<?php echo $circuito['designacao']; ?>" required autofocus>
            </div>

            <div class="form-group">
              <label for"id_unidade">Unidade:</label>
              <select name="id_unidade" required>
              <?php foreach ($unidades as $u): ?>
                <option value='<?php echo $u['id_unidade'];?>'
                <?php echo ($u['id_unidade'] == $circuito['id_unidade']) ? "selected" : "";?>><?php echo $u['unidade_sigla'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group">
              <label for"id_unidade">Operadora:</label>
              <select name="id_operadora" required>
              <?php foreach ($operadoras as $o): ?>
                <option value='<?php echo $o['id_operadora'];?>'
                <?php echo ($o['id_operadora'] == $circuito['id_operadora']) ? "selected" : "";?>><?php echo $o['operadora_sigla'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group">
              <label for="banda">Banda:</label>
              <input type="number" name="banda" id="banda" value="<?php echo $circuito['banda']; ?>">
            </div>

            <div class="form-group">
              <label for="data_ativacao">Data Ativação:</label>
              <input type="date" name="data_ativacao" id="data_ativacao" value="<?php echo $circuito['data_ativacao']; ?>">
            </div>

            <div class="form-group">
              <label for="valor_mensal">Valor Mensal:</label>
              <input type="number" name="valor_mensal" id="valor_mensal" value="<?php echo $circuito['valor_mensal']; ?>">
            </div>

            <div class="form-group">
              <label for"conexao">Conexao:</label>
              <select name="conexao" required>
              <?php foreach ($conexoes as $c): ?>
                <option value='<?php echo $c['id_conexao'];?>'
                <?php echo ($c['id_conexao'] == $circuito['id_conexao']) ? "selected" : ""; ?>><?php echo $c['conexao'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

           <div class="form-group">
             <label for"conexao">Ativo:</label>
             <label class="radio-inline"><input type="radio" name="ativo" value="1"
             <?php echo ($circuito['ativo'] == 1) ? "checked" : ""; ?>>Sim</label>
             <label class="radio-inline"><input type="radio" name="ativo" value="0"
             <?php echo ($circuito['ativo'] == 0) ? "checked" : ""; ?>>Não</label>
           </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>

      <?php
      


    }//fim edita


  } // fim classe

?>
