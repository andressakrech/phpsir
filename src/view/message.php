<?php

class Message {

  function __construct($message=null) {
  // Recebe um array com o tipo do erro em [0] e a mensagem de erro em [1]
    
    switch ($message[0]) {
      case 0:
        // 0 - Sucess
        $this->dismissibleAlertSucess($message[1]);
        break;
      case 1:
        // 1 - Danger (erro)
        $this->dismissibleAlertDanger($message[1]);
        break;
      case 2:
        // 2 - Warning
        $this->dismissibleAlertWarning($message[1]);
        break;
      case 3:
        // 3 - Info
        $this->dismissibleAlertInfo($message[1]);
        break;
      default:
        // default, não faz nada
        break;
    }

  }

  function dismissibleAlertSucess($message) {
    ?>
    <div class="alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?php echo $message; ?>
    </div>
    <?php
  }

  function dismissibleAlertInfo($message) {
    ?>
    <div class="alert alert-info alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?php echo $message; ?>
    </div>
    <?php
  }

  function dismissibleAlertWarning($message) {
    ?>
    <div class="alert alert-warning alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?php echo $message; ?>
    </div>
    <?php

  }

  function dismissibleAlertDanger($message) {
    ?>
    <div class="alert alert-danger alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?php echo $message; ?>
    </div>
    <?php

  }


}
