<?php

  class OperadoraView
  {

    public function cadastra()
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar operadora</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="operadora_nome">Nome*:</label>
              <input type="text" name="operadora_nome" id="operadora_nome" value="" required autofocus>
            </div>

            <div class="form-group">
              <label for="operadora_sigla">Sigla*:</label>
              <input type="text" name="operadora_sigla" id="operadora_sigla" value="" required>
            </div>

            <div class="form-group">
              <label for="site">Site:</label>
              <input type="url" name="site" id="site" value="" placeholder="http://site.com.br">
            </div>

            <div class="form-group">
              <label for="observacao" cols="30">Observação:</label>
              <textarea type="text" name="observacao" id="observacao" value=""></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra


    public function lista($operadoras)
    {
      
      ?>
      <div class="container-fluid">
        <h1 align="right" class="w-25">Operadoras</h1>

        <div class="w-25"><a href=<?php HOME_URI; ?>"index.php?page=operadora&view=cadastra" role="button" class="btn btn-success">+ Cadastrar operadora</a>
        </div><br/>

        <div>
          <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th scope="col">Nome</th>
              <th scope="col">Sigla</th>
              <th scope="col">site</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($operadoras as $k=>$o): ?>
            <tr>
              <td><a href="<?php echo HOME_URI."index.php?page=operadora&view=consulta&id={$o['id_operadora']}"; ?>"><?php echo $o['operadora_nome']; ?></a></td>
              <td><?php echo $o['operadora_sigla']; ?></td>
              <td><?php echo $o['site']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        </div>

      </div><!-- fim container-->

      <?php 

    } // fim lista

    public function consulta($operadora) {

      ?>

      <div class="container-fluid">

        <h3>Operadora</h3>

        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=operadora&view=edita&id={$operadora['id_operadora']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=operadora&view=exclui&id={$operadora['id_operadora']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <p><strong>Nome:</strong></p>
            <p><?php echo $operadora['operadora_nome']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Sigla:</strong></p>
            <p><?php echo $operadora['operadora_sigla']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Site:</strong></p>
            <p><?php echo $operadora['site']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Observação:</strong></p>
            <textarea cols="30" readonly><?php echo $operadora['observacao']; ?></textarea>
          </div>

        </div>

      </div><!-- fim container-->

      <?php 

    } // fim da consulta

    public function edita($operadora)
    {
      
      ?>

      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar operadora</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">

            <div class="form-group">
              <label for="operadora_nome">Nome*:</label>
              <input type="text" name="operadora_nome" id="operadora_nome"
              value="<?php echo $operadora['operadora_nome'];?>" required autofocus>
            </div>

            <div class="form-group">
              <label for="operadora_sigla">Sigla*:</label>
              <input type="text" name="operadora_sigla" id="operadora_sigla"
              value="<?php echo $operadora['operadora_sigla'];?>" required>
            </div>

            <div class="form-group">
              <label for="site">Site:</label>
              <input type="url" name="site" id="site"
              value="<?php echo $operadora['site'];?>">
            </div>

            <div class="form-group">
              <label for="observacao">Observação:</label>
              <textarea type="text" name="observacao" id="observacao" cols="30"><?php echo $operadora['observacao'];?></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>

      <?php
      


    }//fim edita


  } // fim classe

?>
