<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo 'PHPSir - '.( isset($view) ? $view.' ':'' ) . (isset($page) ? $page : 'home') ; ?> </title>

    <!-- Bootstrap core CSS -->
    <link href="src/vendor/bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="src/css/style.css" rel="stylesheet">

  </head>

  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="<?php echo HOME_URI; ?>index.php">PHPSir</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo HOME_URI; ?>index.php">Home </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Instituição
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=instituicao&view=cadastra">Cadastrar</a>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=instituicao&view=lista">Listar</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Unidade
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=unidade&view=cadastra">Cadastrar</a>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=unidade&view=lista">Listar</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Operadora
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=operadora&view=cadastra">Cadastrar</a>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=operadora&view=lista">Listar</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Circuito
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=circuito&view=cadastra">Cadastrar</a>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=circuito&view=lista">Listar</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Equipamento
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=equipamento&view=cadastra">Cadastrar</a>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=equipamento&view=lista">Listar</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=equipamento&view=atualizar">Atualizar equipamentos</a>
          </div>
        </li>


        <li class="nav-item">
          <a class="nav-link" href="<?php echo HOME_URI; ?>index.php?page=interface&view=lista">Interfaces</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Outros
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=enquadramento&view=cadastra">Cadastrar enquadramento</a>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=enquadramento&view=lista">Listar enquadramentos</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=conexao&view=cadastra">Cadastrar conexão</a>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=conexao&view=lista">Listar conexões</a>
<!-- Não há necessidade de alteração da versão na primeira versão
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=versao_snmp&view=cadastra">Cadastrar Versão SNMP</a>
            <a class="dropdown-item" href="<?php echo HOME_URI; ?>index.php?page=versao_snmp&view=lista">Listar versões SNMP</a>
          </div>
-->
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0" action="index.php?page=busca&view=lista" method="post">
        <input class="form-control mr-sm-2" name="busca" id="busca" type="search" placeholder="Busca" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Busca</button>
      </form>
    </div>
  </nav>
