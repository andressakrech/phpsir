<?php

  class EquipamentoView
  {

    public function cadastra($versaoSnmp)
    {
      
      ?>
      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Cadastrar equipamento</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="hostname">Hostname*:</label>
              <input type="text" name="hostname" id="hostname" value="" required autofocus>
            </div>

            <div class="form-group">
              <label for="ipv4">Endereço ipv4*:</label>
              <input type="text" name="ipv4" id="ipv4" value="" required>
            </div>

            <div class="form-group">
              <label for="ipv6">Endereço ipv6:</label>
              <input type="text" name="ipv6" id="ipv6" value="">
            </div>

            <div class="form-group">
              <label for="fabricante">Fabricante:</label>
              <input type="text" name="fabricante" id="fabricante" value="">
            </div>

            <div class="form-group">
              <label for="modelo">Modelo:</label>
              <input type="text" name="modelo" id="modelo" value="">
            </div>

            <div class="form-group">
              <label for="comunidade_snmp">Comunidade SNMP:*</label>
              <input type="text" name="comunidade_snmp" id="comunidade_snmp" value="" required>
            </div>

            <div class="form-group">
              <label for="mascara">Mascara:</label>
              <input type="text" name="mascara" id="mascara" value="" placeholder="unidade;designacao;operadora;info">
            </div>

            <div class="form-group">
              <label for"id_versao_snmp">Versão snmp:</label>
              <select name="id_versao_snmp" required>
              <option value=''>Selecione uma opção</option>
              <?php foreach ($versaoSnmp as $v): ?>
                <option value='<?php echo $v['id_versao_snmp'];?>'><?php echo $v['versao_snmp'];?></option>";
              <?php endforeach; ?>
              </select>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>
      <?php
      


    }//fim cadastra

    public function lista($equipamentos)
    {
      
      ?>
      <div class="container-fluid">
        <h1 align="right" class="w-25">Equipamentos</h1>

        <div class="w-25"><a href=<?php HOME_URI; ?>"index.php?page=equipamento&view=cadastra" role="button" class="btn btn-success">+ Cadastrar equipamento</a>
        </div><br/>

        <div>
          <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th scope="col">Hostname</th>
              <th scope="col">Ipv4</th>
              <th scope="col">Ipv6</th>
              <th scope="col">Fabricante</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($equipamentos as $i): ?>
            <tr>
              <td><a href="<?php echo HOME_URI."index.php?page=equipamento&view=consulta&id={$i['id_equipamento']}"; ?>"><?php echo $i['hostname']; ?></a></td>
              <td><?php echo $i['ipv4']; ?></td>
              <td><?php echo $i['ipv6']; ?></td>
              <td><?php echo $i['fabricante']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        </div>

      </div><!-- fim container-->

      <?php 

    } // fim lista

    public function consulta($equipamento, $versaoSnmp) {

      
      ?>

      <div class="container-fluid">

        <h3>Equipamento</h3>
		
        <div>
          <div class="">
            <a href="<?php echo HOME_URI."index.php?page=equipamento&view=edita&id={$equipamento['id_equipamento']}";?>"
            role="button" class="btn btn-primary" >Editar</a>
            <a href="<?php echo HOME_URI."index.php?page=equipamento&view=exclui&id={$equipamento['id_equipamento']}";?>"
            role="button" class="btn btn-danger">Excluir</a>
            <a href="<?php echo HOME_URI."index.php?page=equipamento&view=atualiza&id={$equipamento['id_equipamento']}";?>"
            role="button" class="btn btn-info">Atualizar interfaces</a>
          </div>
        </div>
		
        <div class="row">
          <div class="col-md-4">
            <p><strong>Hostname:</strong></p>
            <p><?php echo $equipamento['hostname']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Ipv4:</strong></p>
            <p><?php echo $equipamento['ipv4']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Ipv6:</strong></p>
            <p><?php echo $equipamento['ipv6']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Fabricante:</strong></p>
            <p><?php echo $equipamento['fabricante']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Modelo:</strong></p>
            <p><?php echo $equipamento['modelo']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Comunidade SNMP:</strong></p>
            <p><?php echo $equipamento['comunidade_snmp']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Mascara:</strong></p>
            <p><?php echo $equipamento['mascara']; ?></p>
          </div>

          <div class="col-md-4">
            <p><strong>Versão SNMP:</strong></p>
            <p><?php echo $versaoSnmp[$equipamento['id_versao_snmp']]; ?></p>
          </div>

        </div>

      </div><!-- fim container-->

      <?php 

    } // fim da consulta

    public function edita($equipamento, $versoes)
    {
      ?>

      <div class="container-fluid">
        <fieldset class="border p-2">
          <legend class="w-auto">Editar equipamento</legend>

          <div align="left"><p><b>Campos marcados com (*) são obrigatórios.</b></p>
          </div>

          <form action="" method="post" class="inline">
            <div class="form-group">
              <label for="hostname">Hostname*:</label>
              <input type="text" name="hostname" id="hostname"
              value="<?php echo $equipamento['hostname'];?>" required autofocus>
            </div>

            <div class="form-group">
              <label for="ipv4">Ipv4*:</label>
              <input type="text" name="ipv4" id="ipv4"
              value="<?php echo $equipamento['ipv4'];?>" required>
            </div>

            <div class="form-group">
              <label for="ipv6">Ipv6:</label>
              <input type="text" name="ipv6" id="ipv6"
              value="<?php echo $equipamento['ipv6'];?>">
            </div>

            <div class="form-group">
              <label for="fabricante">Fabricante:</label>
              <input type="text" name="fabricante" id="fabricante"
              value="<?php echo $equipamento['fabricante'];?>">
            </div>

            <div class="form-group">
              <label for="modelo">Modelo:</label>
              <input type="text" name="modelo" id="modelo"
              value="<?php echo $equipamento['modelo'];?>">
            </div>

            <div class="form-group">
              <label for="comunidade_snmp">Comunidade SNMP:</label>
              <input type="text" name="comunidade_snmp" id="comunidade_snmp"
              value="<?php echo $equipamento['comunidade_snmp'];?>">
            </div>

            <div class="form-group">
              <label for="mascara">Mascara:</label>
              <input type="text" name="mascara" id="mascara"
              value="<?php echo $equipamento['mascara'];?>">
            </div>

            <div class="form-group">
              <label for"id_versao_snmp">Versao SNMP:</label>
              <select name="id_versao_snmp" required>
              <?php foreach ($versoes as $k=>$v): ?>

                <option value="<?php echo $k;?>"<?php echo ($k == $equipamento['id_versao_snmp']) ? " selected": "" ?>><?php echo $v;?></option>";
              <?php endforeach; ?>
              </select>
            </div>

            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </fieldset>
      </div>

      <?php
      


    }//fim cadastra


  } // fim classe

?>
