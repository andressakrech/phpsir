<?php
include_once(ABSPATH.'/model/equipamento_model.php');
include_once(ABSPATH.'/model/versao_model.php');
include_once(ABSPATH.'/model/interface_model.php');
include_once(ABSPATH.'/model/consulta_snmp.php');
include_once(ABSPATH.'/dao/equipamento_dao.php');
include_once(ABSPATH.'/dao/versao_dao.php');
include_once(ABSPATH.'/dao/interface_dao.php');


$equipamentoDao = new EquipamentoDao();
$versaoDao = new VersaoDao();

$versoes = $versaoDao->listar();
if (isset($_GET['id']))
	$equipamentos = $equipamentoDao->getById($_GET['id']);
else
	$equipamentos = $equipamentoDao->listar();

function getIdCircuito($operadoraApelido, $unidadeApelido, $designacao) {
	/*
	 Realiza um join entre as tabelas Circuito, Equipamento e Unidade caso haja necessidade,
	 e retorna o id_circuito que deve ser usado para relacionar a interface aos circuitos.
	 */
	//$conn->beginTransaction();

	$from = 'SELECT id_circuito FROM Circuito ';
	$where = 'WHERE Circuito.designacao = :designacao';
	if ($operadoraApelido) {
		$from .= ', Operadora ';
		$where .= ' and Operadora.apelido = :operadoraApelido and Circuito.id_operadora = Operadora.id_operadora';
	}
	if ($unidadeApelido) {
		$from .= ', Unidade ';
		$where .= ' and Unidade.apelido = :unidadeApelido and  Circuito.id_unidade = Unidade.id_unidade';
	}

	$consulta = $from."".$where;
	$conn = Registry::getInstance();

	$statement = $conn->prepare($consulta);
	$statement->bindValue(':designacao', $designacao);
	if ($operadoraApelido)
		$statement->bindValue(':operadoraApelido', $operadoraApelido);
	if ($unidadeApelido)
		$statement->bindValue(':unidadeApelido', $unidadeApelido);

	$statement->execute();

	$results = null;

	if ($statement) {
		while($row = $statement->fetch(PDO::FETCH_OBJ)) {
			$results = $row->id_circuito;
			//print_r($row);
		}
	}
	// TODO verificar se sql vai aceitar valor null
	return $results;

}

//print_r($equipamento);
//exit();
?>
<html>
<head>
        <meta charset='UTF-8'>
        <link rel='stylesheet' type='text/css' href=<?php echo RELPATH.'view/css/stylesheet.css'; ?> />
        <title>Atualizar Equipamento</title>

</head>

<body>
        <?php include_once ABSPATH . "/view/header.php"; ?>
        <h1>Equipamentos</h1>

<?php
$arrayVersao = array();
foreach ($versoes as $versao) {
        $arrayVersao[$versao->getIdVersao()] = $versao->getVersao();
}

$arrayConsultaSnmp = array();
// para cada equipamento
foreach ($equipamentos as $key=>$equipamento) {
        // cria uma consulta snmp para o equipamento
        $consultaSnmp = new ConsultaSnmp($equipamento->getHostname(), $equipamento->getComunidade(), $arrayVersao[$equipamento->getIdVersao()]);
        $arrayConsultaSnmp[$equipamento->getIdEquipamento()] = $consultaSnmp->getEquipamentInformation();
	unset($consultaSnmp);
}
        
        foreach ($arrayConsultaSnmp as $key=>$consulta) {
        	
        	foreach ($consulta as $c) {
        		
			//print_r($c);
			
        		$ifName = $c['ifName'];
        		$ifIndex = $c['ifIndex'];
        		$ifDescr = $c['ifDescr'];
        		$ifAlias = $c['ifAlias'];
        		 
        		// quebra o ifalias de acordo com a mescara para descobrir
        		// as informacoes da operadora, unidade e designacao
        		// mascara = (Unidade;Contato;Telefone;Operadora;Designação)
			/*
        		$arrayIfAlias = explode(";", $ifAlias);
        		if (count($arrayIfAlias) == 5) {
        			// pega os dados caso a interface possua informacoes de um circuito
        			$idCircuito = getIdCircuito($arrayIfAlias[3], $arrayIfAlias[0], $arrayIfAlias[4]);
        		} else {
        			$idCircuito = null;
        		}
        		*/
        		$interfaceModel = new InterfaceModel();
        		$interfaceDao = new InterfaceDao();
        		 
        		 
        		//$interfaceModel->setIdCircuito($idCircuito);
        		$interfaceModel->setIdEquipamento($key);
        		$interfaceModel->setIfAlias($ifAlias);
        		$interfaceModel->setIfDescr($ifDescr);
        		$interfaceModel->setIfName($ifName);
        		$interfaceModel->setIfIndex($ifIndex);

			//print_r($interfaceModel);
			//echo "<br/><br/>";
        		 
        		$interfaceDao->inserir($interfaceModel);

        	}
	}

//print_r($arrayConsultaSnmp);

//include_once(ABSPATH.'/model/interface_model.php');
//include_once(ABSPATH.'/dao/interface_dao.php');

//$interfaceModel = new InterfaceModel();
//$interfaceDao = new InterfaceDao();
//$interfaces = $interfaceDao->listar();


if (isset($_GET['id']))
	echo "Equipamento atualizado com sucesso.";
else
	echo "Equipamentos atualizados com sucesso.";



/*
//TODO criar arquivo separado para validacao
// se tiver sido encaminhado por POST verifica se os dados foram preenchidos corretamente e atualiza
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $equipamentoModel = new EquipamentoModel();
        //se os dados obrigatorios foram passados
        if (isset($_POST['hostname']) && isset($_POST['apelido']) && isset($_POST['id_versao'])
        && isset($_GET['id'])) {
                //adiciona os campos obrigatorios
                $equipamentoModel->setHostname($_POST['hostname']);
                $equipamentoModel->setApelido($_POST['apelido']);
                $equipamentoModel->setComunidade($_POST['comunidade']);
                $equipamentoModel->setIdVersao($_POST['id_versao']);
                $equipamentoModel->setIdEquipamento($_GET['id']);
                //insere a equipamento no banco
                $equipamentoDao->atualizar($equipamentoModel);

                echo "<p>Equipamento atualizada com sucesso.</p>";
        } else {
                echo "<p>Por favor preencha todos os campos obrigatórios.</p>";
        }

}

$equipamento = $equipamentoDao->getById($_GET['id']);
*/
?>


                </form>
        </body>
</html>
