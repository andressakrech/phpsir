    <!-- Footer -->
    <footer class="bg-dark fixed-bottom">
      <div class="container">
        <p class="m-0 text-center text-white">PHPSir 2019 - Desenvolvido por: Gustavo Velasques Dreier</p>
      </div>
    </footer>
    <!-- Bootstrap core JavaScript -->

    <script src="src/vendor/jquery/jquery-3.4.1.min.js"></script>
    <script src="src/vendor/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
