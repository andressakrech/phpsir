<?php

//registra a conexão com o DB

class Registry {

  private static $instance;

  protected function __construct() {

  }

  public static function getInstance() {
    if ( !self::$instance )
    self::$instance = new PDO('mysql:host='.HOSTNAME.';port='.PORT.';dbname='.DB_NAME.';charset='.DB_CHARSET.'', DB_USER, DB_PASS);
    self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return self::$instance;
  }

}
?>

