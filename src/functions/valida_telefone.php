<?php

function validaTelefone($fone) {
  // remove caracteres nao numericos
  $fone = preg_replace("/[^0-9]/", "", $fone);
  switch (strlen($fone)) {
    case 11://celular com ddd ou 0800
      if (($fone[0].$fone[1].$fone[2].$fone[3]) == "0800") ://se 0800
        return substr($fone, 0, 4) ." ".substr($fone,4, 3)." ".substr($fone,7);
      endif;
      return "(". substr($fone, 0, 2) .")".substr($fone,2, 5)."-".substr($fone,7);
    case 10://telefone com ddd
      return "(". substr($fone, 0, 2) .")".substr($fone,2, 4)."-".substr($fone,6);
    case 9://celular sem o ddd. Adiciona ddd default (51)
      return "(51)".substr($fone,0, 5)."-".substr($fone,5);
    case 8://telefone sem o ddd. Adiciona ddd default (51)
      return "(51)".substr($fone,0, 4)."-".substr($fone,4);
    default://invalido
      return null;
  }
}

