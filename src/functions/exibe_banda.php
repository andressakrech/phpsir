<?php

function exibeBanda($banda) {
  // se for giga ou maior exibe me Gbps
  if ($banda >= 1000000000000) {
    return ($banda/1000000000000)."Tbps";
  } elseif ($banda >= 1000000000) {
    return ($banda/1000000000)."Gbps";
  } elseif ($banda >= 1000000) {
    return ($banda/1000000)."Mbps";
  } else {
    return ($banda/1000)."Kbps";
  }
}

