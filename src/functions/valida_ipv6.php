<?php

function validaIpv6($ipv6) {
  return filter_var($ipv6, FILTER_VALIDATE_IP,FILTER_FLAG_IPV6);
}
