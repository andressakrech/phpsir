<?php

function validaIpv4($ipv4) {
  return filter_var($ipv4, FILTER_VALIDATE_IP,FILTER_FLAG_IPV4);
}
