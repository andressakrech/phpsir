<?php

include_once(ABSPATH.'/functions/valida_ipv4.php');
include_once(ABSPATH.'/functions/valida_ipv6.php');

function discoverIpv4($version, $community, $host) {
  $oid = 'iso.3.6.1.2.1.4.34.1.3.1.4';
  $comando = "snmpbulkwalk -v{$version} -c {$community} {$host} {$oid}";

  exec($comando, $saida, $status);
  // se o comando funcionou e a mib existir no equipamento
  if ((!$status) && (strpos($saida[0], 'No Such Object available on this agent at this OID') === false)) {
    foreach($saida as $k=>$s) {
      //remove o oid do inicio da string
      $s = str_replace("{$oid}.", "", $s);
      //adiciona ao vetor endereco o array com o endereco ipv4 [0] e o indice em [1]
      $endereco[] = explode(' = INTEGER: ', $s);

      // $ipv4[$ifIndex] = $ipv4Addr
      $ipv4[$endereco[$k][1]][] = $endereco[$k][0];

    } // fim foreach
    return $ipv4;
  }//fim if
  return array();
}//fim function

function discoverIpv6($version, $community, $host) {
  $oid='iso.3.6.1.2.1.55.1.8.1.5';
  $comando = "snmpbulkwalk -v{$version} -c {$community} {$host} {$oid}";
  // exec retorna status 0 quando comando funciona - obs: O snmp2_walk() nao retornava informacoes de ipv6
  exec($comando, $saida, $status);

  //Se o comando funcionou (!$status) e o agente possuia objetos
  if (!$status && (strpos($saida[0],'No Such Object available on this agent at this OID') === false)) {
    ////para cada linha
    foreach($saida as $k=>$s) {
      //remove o oid do inicio da linha
      $s = str_replace("{$oid}.", "", $s);
      //divide o vetor removendo o dado irrelevante (que ficará na posição 1)
      $endereco[] = explode(' = INTEGER: ', $s);
      //separa o ifIndex (em 0) dos octetos em decimal (ex: endereco[$k][0] = 523.254.128.0.0.0.0.0.0.62.138.176.255.254.94.214.156)
      $parts = explode(".", $endereco[$k][0]);

      $ipv6Addr = "";
      // converte o valor decimal para hexa
      for ($i = 1; $i < count($parts);$i++):
        // se menor que 10 em hexadecimal adiciona o zero na frente
        $parts[$i] = $parts[$i] < 16 ? "0".dechex($parts[$i]) : dechex($parts[$i]);

        //o separador sempre termina em par, com exceção do final
        if ( (($i%2) == 0) && ($i < (count($parts) - 1)) ):
          $ipv6Addr .= $parts[$i].":";
        else:
          $ipv6Addr .= $parts[$i];
        endif;
      endfor;

      //if (isset($ipv6[$parts[0]]))://cria ou adiciona ao array com o indice do ifIndex ($parts[0])
      //  array_push($ipv6[$parts[0]], $ipv6Addr);
      //else:
      $ipv6[$parts[0]][] = (validaIpv6($ipv6Addr) ? (inet_ntop(inet_pton($ipv6Addr))) : "");
      //endif;

    }//fim foreach

  } else {
      return array();
  }

  return $ipv6;

}

function buscaSnmp($version, $community, $host) {
  $ifIndex = snmp2_walk ($host, $community, '1.3.6.1.2.1.2.2.1.1');
  $ifName = snmp2_walk ($host, $community,'1.3.6.1.2.1.31.1.1.1.1');
  $ifDescription = snmp2_walk ($host, $community,'1.3.6.1.2.1.2.2.1.2');
  $ifAlias = snmp2_walk ($host, $community, '1.3.6.1.2.1.31.1.1.1.18');
  $ipv4 = discoverIpv4($version, $community, $host);
  $ipv6 = discoverIpv6($version, $community, $host);
  //print_r(discoverIpv4($version, $community, $host));
  //print_r(discoverIpv6($version, $community, $host));

  foreach ($ifIndex as $key=>$value) {

  // O indice é o próprio ifIndex
  $indice = str_replace('INTEGER: ', '', $ifIndex[$key]);
  $eqtoInterface[$indice]['ifIndex'] = str_replace('INTEGER: ', '', $ifIndex[$key]);;//value
  $eqtoInterface[$indice]['ifName'] = str_replace('STRING: ', '', $ifName[$key]);
  $eqtoInterface[$indice]['ifDescription'] = str_replace('STRING: ', '', $ifDescription[$key]);
  $eqtoInterface[$indice]['ifAlias'] = str_replace('STRING: ', '', $ifAlias[$key]);
  if (isset($ipv4[$indice])) {
    foreach ($ipv4[$indice] as $e):
      $eqtoInterface[$indice]['ipv4'][] = $e;
    endforeach;
  } else {
    $eqtoInterface[$indice]['ipv4'] = array();
  }

  if (isset($ipv6[$indice])) {
    foreach ($ipv6[$indice] as $e):
      $eqtoInterface[$indice]['ipv6'][] = $e;
    endforeach;
  } else {
    $eqtoInterface[$indice]['ipv6'] = array();
  }
  //$eqtoInterface[$indice]['ipv4'] = isset($ipv4[$indice]);
  //$eqtoInterface[$indice]['ipv6'] = isset($ipv6[$indice]);

  }
  return $eqtoInterface;
}

