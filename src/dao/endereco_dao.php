<?php

include_once(ABSPATH.'/model/endereco_model.php');

class EnderecoDao
{

  private $conn;

  public function __construct()
  {
    $this->conn = Registry::getInstance();
  }

  public function inserir(EnderecoModel $endereco)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO endereco (endereco, bairro, cidade, cep, coordenada)
        VALUES (:endereco, :bairro, :cidade, :cep, :coordenada)
        ON DUPLICATE KEY UPDATE endereco = :endereco, cidade = :cidade, coordenada = :coordenada'
      );

      $stmt->bindValue(':endereco', $endereco->getEndereco());
      $stmt->bindValue(':bairro', $endereco->getBairro());
      $stmt->bindValue(':cidade', $endereco->getCidade());
      $stmt->bindValue(':cep', $endereco->getCep());
      $stmt->bindValue(':coordenada', $endereco->getCoordenada());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
    }

  }

  public function excluir(int $idEndereco)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM endereco WHERE id_endereco= :idEndereco'
      );

      $stmt->bindValue(':idEndereco', $idEndereco);
      $stmt->execute();

      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar()
  {
    $statement = $this->conn->query(
      'SELECT * FROM endereco'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement)
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $endereco = new EnderecoModel();

        $endereco->setIdEndereco($row->id_endereco);
        $endereco->setEndereco($row->endereco);
        $endereco->setBairro($row->bairro);
        $endereco->setCidade($row->cidade);
        $endereco->setCep($row->cep);
        $endereco->setCoordenada($row->coordenada);

        $results[] = $endereco;
      }
    }

    return $results;
  }

  public function atualizar(EnderecoModel $endereco) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE endereco SET endereco = :endereco, bairro = :bairro, cidade = :cidade, cep = :cep, coordenada = :coordenada
        WHERE id_endereco = :id_endereco'
      );

      $stmt->bindValue(':endereco', $endereco->getEndereco());
      $stmt->bindValue(':bairro', $endereco->getBairro());
      $stmt->bindValue(':cidade', $endereco->getCidade());
      $stmt->bindValue(':cep', $endereco->getCep());
      $stmt->bindValue(':coordenada', $endereco->getCoordenada());
      $stmt->bindValue(':id_endereco', $endereco->getIdEndereco());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idEndereco) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM endereco WHERE id_endereco = :idEndereco'
      );
      $statement->bindValue(':idEndereco', $idEndereco);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }

    return $this->processaResultados($statement);
  }

  public function getByCidade(string $cidade) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM cidade WHERE cidade = :cidade'
      );
      $statement->bindValue(':cidade', $cidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }
    return $this->processaResultados($statement);
  }

}
