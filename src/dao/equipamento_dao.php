<?php

include_once(ABSPATH.'/model/equipamento_model.php');

class EquipamentoDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(EquipamentoModel $equipamento)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO equipamento (hostname, ipv4, ipv6, fabricante, modelo, comunidade_snmp, mascara, id_versao_snmp)
        VALUES (:hostname, :ipv4, :ipv6, :fabricante, :modelo, :comunidadeSnmp, :mascara, :idVersaoSnmp)'
      );

      $stmt->bindValue(':hostname', $equipamento->getHostname());
      $stmt->bindValue(':ipv4', $equipamento->getIpv4());
      $stmt->bindValue(':ipv6', $equipamento->getIpv6());
      $stmt->bindValue(':fabricante', $equipamento->getFabricante());
      $stmt->bindValue(':modelo', $equipamento->getModelo());
      $stmt->bindValue(':comunidadeSnmp', $equipamento->getComunidadeSnmp());
      $stmt->bindValue(':mascara', $equipamento->getMascara());
      $stmt->bindValue(':idVersaoSnmp', $equipamento->getIdVersaoSnmp());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
    }

  }

  public function excluir(int $idEquipamento) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM equipamento WHERE id_equipamento= :idEquipamento'
      );

      $stmt->bindValue(':idEquipamento', $idEquipamento);
      $stmt->execute();

      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
	{
    $statement = $this->conn->query(
      'SELECT * FROM equipamento'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
	
	{
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $equipamento = new EquipamentoModel();

        $equipamento->setIdEquipamento($row->id_equipamento);
        $equipamento->setHostname($row->hostname);
        $equipamento->setIpv4($row->ipv4);
        $equipamento->setIpv6($row->ipv6);
        $equipamento->setFabricante($row->fabricante);
        $equipamento->setModelo($row->modelo);
        $equipamento->setComunidadeSnmp($row->comunidade_snmp);
        $equipamento->setMascara($row->mascara);
        $equipamento->setIdVersaoSnmp($row->id_versao_snmp);

        $results[] = $equipamento;
      }
    }

    return $results;
  }

  public function atualizar(EquipamentoModel $equipamento) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
      'UPDATE equipamento
      SET hostname = :hostname, ipv4 = :ipv4, ipv6 = :ipv6, fabricante = :fabricante, modelo = :modelo,
      comunidade_snmp = :comunidade_snmp, mascara = :mascara, id_versao_snmp = :id_versao_snmp
      WHERE id_equipamento = :id_equipamento'
      );

      $stmt->bindValue(':hostname', $equipamento->getHostname());
      $stmt->bindValue(':ipv4', $equipamento->getIpv4());
      $stmt->bindValue(':ipv6', $equipamento->getIpv6());
      $stmt->bindValue(':fabricante', $equipamento->getFabricante());
      $stmt->bindValue(':modelo', $equipamento->getModelo());
      $stmt->bindValue(':comunidade_snmp', $equipamento->getComunidadeSnmp());
      $stmt->bindValue(':id_versao_snmp', $equipamento->getIdVersaoSnmp());
      $stmt->bindValue(':mascara', $equipamento->getMascara());
      $stmt->bindValue(':id_equipamento', $equipamento->getIdEquipamento());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idEquipamento) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM equipamento WHERE id_equipamento = :idEquipamento'
      );
      $statement->bindValue(':idEquipamento', $idEquipamento);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByHostname(string $hostname) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM equipamento WHERE hostname = :nome'
      );
      $statement->bindValue(':nome', $hostname);
      $statement->execute();


    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

  public function getLikeHostname(string $hostname) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM equipamento WHERE hostname LIKE :nome'
      );
      $statement->bindValue(':nome', '%'.$hostname.'%');
      $statement->execute();


    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
