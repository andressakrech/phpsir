<?php

include_once(ABSPATH.'/model/unidade_model.php');
include_once(ABSPATH.'/dao/endereco_dao.php');

class UnidadeDao
{

  private $conn;

  public function __construct()
  {
    $this->conn = Registry::getInstance();
  }

  public function inserir(UnidadeModel $unidade)
  {

    $enderecoDao = new EnderecoDao();
    // realiza a inserção do endereço e retorna o id do endereco cadastrado
    $idEndereco = $enderecoDao->inserir($unidade->getEndereco());

    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO unidade (unidade_nome, unidade_sigla, data_conexao, observacao, id_instituicao, id_endereco)
        VALUES (:unidade_nome, :unidade_sigla, :data_conexao, :observacao, :id_instituicao, :id_endereco)'
      );

      $stmt->bindValue(':unidade_nome', $unidade->getUnidadeNome());
      $stmt->bindValue(':unidade_sigla', $unidade->getUnidadeSigla());
      $stmt->bindValue(':data_conexao', $unidade->getDataConexao());
      $stmt->bindValue(':observacao', $unidade->getObservacao());
      $stmt->bindValue(':id_instituicao', $unidade->getIdInstituicao());
      $stmt->bindValue(':id_endereco', $idEndereco);
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
    }

  }

  public function excluir(int $idUnidade)
  {
    /*
    // remove o endereco
    $enderecoDao = new enderecoDao();
    $unidadeDao = new UnidadeDao();
    $unidade = new $unidadeDao->getById($idUnidade)[0];
    $enderecoDao->excluir($unidade->getEndereco()->getIdEndereco());
    */
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM unidade WHERE id_unidade= :idUnidade'
      );

      $stmt->bindValue(':idUnidade', $idUnidade);
      $stmt->execute();
      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar()
	{
    $statement = $this->conn->query(
      'SELECT * FROM unidade ORDER BY unidade_sigla'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement)

	{
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        //print_r($row);
        $unidade = new UnidadeModel();

        $unidade->setIdUnidade($row->id_unidade);
        $unidade->setUnidadeNome($row->unidade_nome);
        $unidade->setUnidadeSigla($row->unidade_sigla);
        $unidade->setDataConexao($row->data_conexao);
        $unidade->setObservacao($row->observacao);
        $unidade->setIdInstituicao($row->id_instituicao);

        $enderecoDao = new EnderecoDao();
        $endereco = $enderecoDao->getById($row->id_endereco);

        $unidade->setEndereco($endereco[0]);

        $results[] = $unidade;
      }
    }

    return $results;
  }

  public function atualizar(UnidadeModel $unidade) {

    $enderecoDao = new EnderecoDao();
    // atualiza o endereço
    $idEndereco = $enderecoDao->atualizar($unidade->getEndereco());

    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE unidade
        SET unidade_nome = :nome, unidade_sigla = :unidade_sigla, data_conexao = :data_conexao, observacao = :observacao,
        id_instituicao = :id_instituicao, id_endereco = :id_endereco
        WHERE id_unidade = :id_unidade'
      );

      $stmt->bindValue(':nome', $unidade->getUnidadeNome());
      $stmt->bindValue(':unidade_sigla', $unidade->getUnidadeSigla());
      $stmt->bindValue(':data_conexao', $unidade->getDataConexao());
      $stmt->bindValue(':observacao', $unidade->getObservacao());
      $stmt->bindValue(':id_instituicao', $unidade->getIdInstituicao());
      $stmt->bindValue(':id_endereco', $unidade->getEndereco()->getIdEndereco());
      $stmt->bindValue(':id_unidade', $unidade->getIdUnidade());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idUnidade) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM unidade WHERE id_unidade = :idUnidade'
      );
      $statement->bindValue(':idUnidade', $idUnidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getBySigla(string $siglaUnidade) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM unidade WHERE unidade_sigla = :sigla'
      );
      $statement->bindValue(':sigla', $siglaUnidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }
    return $this->processaResultados($statement);
  }

  public function getByIdInstituicao(int $idInstituicao) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM unidade WHERE id_instituicao = :idInstituicao'
      );
      $statement->bindValue(':idInstituicao', $idInstituicao);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }

    return $this->processaResultados($statement);
  }

  public function getLikeSigla(string $siglaUnidade) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM unidade WHERE unidade_sigla LIKE :sigla'
      );
      $statement->bindValue(':sigla', "%{$siglaUnidade}%");
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }
    return $this->processaResultados($statement);
  }

}
