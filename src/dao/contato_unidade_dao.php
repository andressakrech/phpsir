<?php

include_once(ABSPATH.'/model/contato_unidade_model.php');

class ContatoUnidadeDao
{

  private $conn;

  public function __construct()
  {
    $this->conn = Registry::getInstance();
  }

  public function inserir(ContatoUnidadeModel $contatoUnidade)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO contato_unidade (nome, observacao, tecnico, seguranca, administrativo, email, telefone, id_unidade) 
        VALUES (:nome, :observacao, :tecnico, :seguranca, :administrativo, :email, :telefone, :id_unidade)'
      );

      $stmt->bindValue(':nome', $contatoUnidade->getNome());
      $stmt->bindValue(':observacao', $contatoUnidade->getObservacao());
      $stmt->bindValue(':tecnico', $contatoUnidade->getTecnico());
      $stmt->bindValue(':seguranca', $contatoUnidade->getSeguranca());
      $stmt->bindValue(':administrativo', $contatoUnidade->getAdministrativo());
      $stmt->bindValue(':email', $contatoUnidade->getEmail());
      $stmt->bindValue(':telefone', $contatoUnidade->getTelefone());
      $stmt->bindValue(':id_unidade', $contatoUnidade->getIdUnidade());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idContatoUnidade) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM contato_unidade WHERE id_contato_unidade= :idContatoUnidade'
      );

      $stmt->bindValue(':idContatoUnidade', $idContatoUnidade);
      $stmt->execute();

      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM contato_unidade'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $contatoUnidade = new ContatoUnidadeModel();

        $contatoUnidade->setIdContatoUnidade($row->id_contato_unidade);
        $contatoUnidade->setNome($row->nome);
        $contatoUnidade->setObservacao($row->observacao);
        $contatoUnidade->setTecnico($row->tecnico);
        $contatoUnidade->setSeguranca($row->seguranca);
        $contatoUnidade->setAdministrativo($row->administrativo);
        $contatoUnidade->setEmail($row->email);
        $contatoUnidade->setTelefone($row->telefone);
        $contatoUnidade->setIdUnidade($row->id_unidade);

        $results[] = $contatoUnidade;
      }
    }

    return $results;
  }

  public function atualizar(ContatoUnidadeModel $contatoUnidade) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE contato_unidade 
        SET nome = :nome, observacao = :observacao, tecnico = :tecnico, seguranca = :seguranca,
        administrativo = :administrativo, email = :email, telefone = :telefone, id_unidade =:id_unidade
        WHERE id_contato_unidade = :id_contato_unidade'
      );

      $stmt->bindValue(':nome', $contatoUnidade->getNome());
      $stmt->bindValue(':observacao', $contatoUnidade->getObservacao());
      $stmt->bindValue(':tecnico', $contatoUnidade->getTecnico());
      $stmt->bindValue(':seguranca', $contatoUnidade->getSeguranca());
      $stmt->bindValue(':administrativo', $contatoUnidade->getAdministrativo());
      $stmt->bindValue(':email', $contatoUnidade->getEmail());
      $stmt->bindValue(':telefone', $contatoUnidade->getTelefone());
      $stmt->bindValue(':id_unidade', $contatoUnidade->getIdUnidade());
      $stmt->bindValue(':id_contato_unidade', $contatoUnidade->getIdContatoUnidade());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idContatoUnidade) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM contato_unidade WHERE id_contato_unidade = :idContatoUnidade'
      );
      $statement->bindValue(':idContatoUnidade', $idContatoUnidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }

    return $this->processaResultados($statement);
  }

  public function getByNome(string $nomeContatoUnidade) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM contato_unidade WHERE contato_unidade_nome = :nome'
      );
      $statement->bindValue(':nome', $nomeContatoUnidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }
    return $this->processaResultados($statement);
  }

  public function getByIdUnidade(int $idUnidade) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM contato_unidade WHERE id_unidade = :idUnidade'
      );
      $statement->bindValue(':idUnidade', $idUnidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }

    return $this->processaResultados($statement);
  }

}
