<?php

include_once(ABSPATH.'/model/operadora_model.php');

class OperadoraDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(OperadoraModel $operadora)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO operadora (operadora_nome, operadora_sigla, site, observacao)
        VALUES (:operadora_nome, :operadora_sigla, :site, :observacao)'
      );

      $stmt->bindValue(':operadora_nome', $operadora->getOperadoraNome());
      $stmt->bindValue(':operadora_sigla', $operadora->getOperadoraSigla());
      $stmt->bindValue(':site', $operadora->getSite());
      $stmt->bindValue(':observacao', $operadora->getObservacao());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idOperadora) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM operadora WHERE id_operadora= :idOperadora'
      );

      $stmt->bindValue(':idOperadora', $idOperadora);
      $stmt->execute();

      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
	{
    $statement = $this->conn->query(
      'SELECT * FROM operadora'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
	
	{
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $operadora = new OperadoraModel();

        $operadora->setIdOperadora($row->id_operadora);
        $operadora->setOperadoraNome($row->operadora_nome);
        $operadora->setOperadoraSigla($row->operadora_sigla);
        $operadora->setSite($row->site);
        $operadora->setObservacao($row->observacao);

        $results[] = $operadora;
      }
    }

    return $results;
  }

  public function atualizar(OperadoraModel $operadora) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE operadora 
        SET operadora_nome = :operadora_nome, operadora_sigla = :operadora_sigla, site = :site, observacao = :observacao
        WHERE id_operadora = :id_operadora'
      );

      $stmt->bindValue(':operadora_nome', $operadora->getOperadoraNome());
      $stmt->bindValue(':operadora_sigla', $operadora->getOperadoraSigla());
      $stmt->bindValue(':site', $operadora->getSite());
      $stmt->bindValue(':observacao', $operadora->getObservacao());
      $stmt->bindValue(':id_operadora', $operadora->getIdOperadora());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idOperadora) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM operadora WHERE id_operadora = :idOperadora'
      );
      $statement->bindValue(':idOperadora', $idOperadora);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByOperadoraSigla(string $operadoraSigla) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM operadora WHERE operadora_sigla = :operadora_sigla'
      );
      $statement->bindValue(':operadora_sigla', $operadoraSigla);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
