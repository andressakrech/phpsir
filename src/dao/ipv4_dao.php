<?php

include_once(ABSPATH.'/model/ipv4_model.php');

class Ipv4Dao
{

  private $conn;

  public function __construct()
  {
    $this->conn = Registry::getInstance();
  }

  public function inserir(Ipv4Model $ipv4)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO ipv4 (endereco_ipv4, id_interface) VALUES (:enderecoIpv4, :idInterface)'
      );

      $stmt->bindValue(':enderecoIpv4', $ipv4->getEnderecoIpv4());
      $stmt->bindValue(':idInterface', $ipv4->getIdInterface());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idIpv4) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM ipv4 WHERE id_ipv4= :idIpv4'
      );

      $stmt->bindValue(':idIpv4', $idIpv4);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM ipv4'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $ipv4 = new Ipv4Model();

        $ipv4->setIdIpv4($row->id_ipv4);
        $ipv4->setEnquadramentoNome($row->endereco_ipv4);
        $ipv4->setIdInterface($row->id_interface);

        $results[] = $ipv4;
      }
    }

    return $results;
  }

  public function atualizar(Ipv4Model $ipv4) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE ipv4
        SET endereco_ipv4 = :nome, id_interface = :idInterface
        WHERE id_ipv4 = :id_ipv4'
      );

      $stmt->bindValue(':nome', $ipv4->getEnderecoIpv4());
      $stmt->bindValue(':id_interface', $ipv4->getIdInterface());
      $stmt->bindValue(':id_ipv4', $ipv4->getIdIpv4());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idIpv4) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM ipv4 WHERE id_ipv4 = :idIpv4'
      );
      $statement->bindValue(':idIpv4', $idIpv4);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByIdInterface(int $idInterface) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM ipv4 WHERE id_interface = :idInterface'
      );
      $statement->bindValue(':idInterface', $idInterface);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByEnderecoIpv4(string $endereco) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM ipv4 WHERE endereco_ipv4 = :endereco'
      );
      $statement->bindValue(':endereco', $endereco);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
