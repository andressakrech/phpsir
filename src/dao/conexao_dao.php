<?php

include_once(ABSPATH.'/model/conexao_model.php');

class ConexaoDao
{

  private $conn;

  public function __construct()
  {
    $this->conn = Registry::getInstance();
  }

  public function inserir(ConexaoModel $conexao)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO conexao (conexao) VALUES (:nome)'
      );

      $stmt->bindValue(':nome', $conexao->getConexao());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
    }
  }

  public function excluir(int $idConexao)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM conexao WHERE id_conexao= :idConexao'
      );

      $stmt->bindValue(':idConexao', $idConexao);
      $stmt->execute();

      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      if (DEBUG) {
        echo "Erro: $e";
      }
      $this->conn->rollback();
    }
  }



  public function listar()
        {
    $statement = $this->conn->query(
      'SELECT * FROM conexao'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement)

        {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $conexao = new ConexaoModel();

        $conexao->setIdConexao($row->id_conexao);
        $conexao->setConexao($row->conexao);

        $results[] = $conexao;
      }
    }

    return $results;
  }

  public function atualizar(ConexaoModel $conexao) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE conexao SET conexao = :nome WHERE id_conexao = :id_conexao'
      );

      $stmt->bindValue(':nome', $conexao->getConexao());
      $stmt->bindValue(':id_conexao', $conexao->getIdConexao());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
    }
  }

  public function getById(int $idConexao) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM conexao WHERE id_conexao = :idConexao'
      );
      $statement->bindValue(':idConexao', $idConexao);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByNome(string $nomeConexao) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM conexao WHERE conexao = :nome'
      );
      $statement->bindValue(':nome', $nomeConexao);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
