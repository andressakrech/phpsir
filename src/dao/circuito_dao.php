<?php

include_once(ABSPATH.'/model/circuito_model.php');

class CircuitoDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(CircuitoModel $circuito)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO circuito (id_operadora, id_unidade, designacao, banda, data_ativacao, valor_mensal, ativo, id_conexao)
        VALUES (:id_operadora, :id_unidade, :designacao, :banda, :data_ativacao, :valor_mensal, :ativo, :id_conexao)'
      );

      $stmt->bindValue(':id_operadora', $circuito->getIdOperadora());
      $stmt->bindValue(':id_unidade', $circuito->getIdUnidade());
      $stmt->bindValue(':designacao', $circuito->getDesignacao());
      $stmt->bindValue(':banda', $circuito->getBanda());
      $stmt->bindValue(':data_ativacao', $circuito->getDataAtivacao());
      $stmt->bindValue(':valor_mensal', $circuito->getValorMensal());
      $stmt->bindValue(':ativo', $circuito->getAtivo());
      $stmt->bindValue(':id_conexao', $circuito->getIdConexao());
      $stmt->execute();
      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }
  }

  public function excluir(int $idCircuito) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM circuito WHERE id_circuito= :idCircuito'
      );

      $stmt->bindValue(':idCircuito', $idCircuito);
      $stmt->execute();

      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
	{
    $statement = $this->conn->query(
      'SELECT * FROM circuito'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
	
	{
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $circuito = new CircuitoModel();

        $circuito->setIdCircuito($row->id_circuito);
        $circuito->setIdOperadora($row->id_operadora);
        $circuito->setIdUnidade($row->id_unidade);
        $circuito->setDesignacao($row->designacao);
        $circuito->setBanda($row->banda);
        $circuito->setDataAtivacao($row->data_ativacao);
        $circuito->setValorMensal($row->valor_mensal);
        $circuito->setAtivo($row->ativo);
        $circuito->setIdConexao($row->id_conexao);

        $results[] = $circuito;
      }
    }

    return $results;
  }

  public function atualizar(CircuitoModel $circuito) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE circuito 
        SET id_operadora = :id_operadora, id_unidade = :id_unidade, designacao = :designacao, 
        banda = :banda, data_ativacao = :data_ativacao, valor_mensal = :valor_mensal, ativo = :ativo, id_conexao = :id_conexao
        WHERE id_circuito = :id_circuito'
      );

      $stmt->bindValue(':id_operadora', $circuito->getIdOperadora());
      $stmt->bindValue(':id_unidade', $circuito->getIdUnidade());
      $stmt->bindValue(':designacao', $circuito->getDesignacao());
      $stmt->bindValue(':banda', $circuito->getBanda());
      $stmt->bindValue(':data_ativacao', $circuito->getDataAtivacao());
      $stmt->bindValue(':valor_mensal', $circuito->getValorMensal());
      $stmt->bindValue(':ativo', $circuito->getAtivo());
      $stmt->bindValue(':id_conexao', $circuito->getIdConexao());
      $stmt->bindValue(':id_circuito', $circuito->getIdCircuito());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idCircuito) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM circuito WHERE id_circuito = :idCircuito'
      );
      $statement->bindValue(':idCircuito', $idCircuito);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return false;
    }

    return $this->processaResultados($statement);
  }

  public function getByDesignacao(string $designacao) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM circuito WHERE designacao = :designacao'
      );
      $statement->bindValue(':designacao', $designacao);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return false;
    }
    return $this->processaResultados($statement);
  }

}
