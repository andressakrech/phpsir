<?php

include_once(ABSPATH.'/model/telefone_contato_unidade_model.php');

class TelefoneContatoUnidadeDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(TelefoneContatoUnidadeModel $telefoneContatoUnidade)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO telefone_contato_unidade (ddd, numero, id_contato_unidade) 
        VALUES (:ddd, :numero, :id_contato_unidade)'
      );

      $stmt->bindValue(':ddd', $telefoneContatoUnidade->getDdd());
      $stmt->bindValue(':numero', $telefoneContatoUnidade->getNumero());
      $stmt->bindValue(':id_contato_unidade', $telefoneContatoUnidade->getIdContatoUnidade());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idTelefoneContatoUnidade) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM telefone_contato_unidade WHERE id_telefone_contato_unidade = :idTelefoneContatoUnidade'
      );

      $stmt->bindValue(':idTelefoneContatoUnidade', $idTelefoneContatoUnidade);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM telefone_contato_unidade'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $telefoneContatoUnidade = new TelefoneContatoUnidadeModel();

        $telefoneContatoUnidade->setIdTelefoneContatoUnidade($row->id_telefone_contato_unidade);
        $telefoneContatoUnidade->setDdd($row->ddd);
        $telefoneContatoUnidade->setNumero($row->numero);
        $telefoneContatoUnidade->setIdContatoUnidade($row->id_contato_unidade);

        $results[] = $telefoneContatoUnidade;
      }
    }

    return $results;
  }

  public function atualizar(TelefoneContatoUnidadeModel $telefone_contato_unidade) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE telefone_contato_unidade 
        SET ddd = :ddd, numero = :numero, id_contato_unidade = :id_contato_unidade
        WHERE id_telefone_contato_unidade = :id_telefone_contato_unidade'
      );

      $stmt->bindValue(':ddd', $telefone_contato_unidade->getDdd());
      $stmt->bindValue(':numero', $telefone_contato_unidade->getNumero());
      $stmt->bindValue(':id_contato_unidade', $telefone_contato_unidade->getIdContatoUnidade());
      $stmt->bindValue(':id_telefone_contato_unidade', $telefone_contato_unidade->getIdTelefoneContatoUnidade());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idTelefoneContatoUnidade) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM telefone_contato_unidade WHERE id_telefone_contato_unidade = :idTelefoneContatoUnidade'
      );
      $statement->bindValue(':idTelefoneContatoUnidade', $idTelefoneContatoUnidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

}
