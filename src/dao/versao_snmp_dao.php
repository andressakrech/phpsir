<?php

include_once(ABSPATH.'/model/versao_snmp_model.php');

class VersaoSnmpDao
{

  private $conn;

  public function __construct()
        {
    $this->conn = Registry::getInstance();
  }

  public function inserir(VersaoSnmpModel $versaoSnmp)
        {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO versao_snmp (versao_snmp) VALUES (:nome)'
      );

      $stmt->bindValue(':nome', $versaoSnmp->getVersaoSnmp());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idVersaoSnmp)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM versao_snmp WHERE id_versao_snmp= :idVersaoSnmp'
      );

      $stmt->bindValue(':idVersaoSnmp', $idVersaoSnmp);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar()
        {
    $statement = $this->conn->query(
      'SELECT * FROM versao_snmp'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement)

        {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $versaoSnmp = new VersaoSnmpModel();

        $versaoSnmp->setIdVersaoSnmp($row->id_versao_snmp);
        $versaoSnmp->setVersaoSnmp($row->versao_snmp);

        $results[] = $versaoSnmp;
      }
    }

    return $results;
  }

  public function atualizar(VersaoSnmpModel $versaoSnmp) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE versao_snmp SET versao_snmp = :versaoSnmp WHERE id_versao_snmp = :idVersaoSnmp'
      );

      $stmt->bindValue(':versaoSnmp', $versaoSnmp->getVersaoSnmp());
      $stmt->bindValue(':idVersaoSnmp', $versaoSnmp->getIdVersaoSnmp());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idVersaoSnmp) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM versao_snmp WHERE id_versao_snmp = :idVersaoSnmp'
      );
      $statement->bindValue(':idVersaoSnmp', $idVersaoSnmp);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByNome(string $nomeVersaoSnmp) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM versao_snmp WHERE versao_snmp = :nome'
      );
      $statement->bindValue(':nome', $nomeVersaoSnmp);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
