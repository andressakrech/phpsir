<?php

include_once(ABSPATH.'/model/enquadramento_model.php');

class EnquadramentoDao
{

  private $conn;

  public function __construct()
  {
    $this->conn = Registry::getInstance();
  }

  public function inserir(EnquadramentoModel $enquadramento)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO enquadramento (enquadramento_nome) VALUES (:nome)'
      );

      $stmt->bindValue(':nome', $enquadramento->getEnquadramentoNome());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
    }
  }

  public function excluir(int $idEnquadramento) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM enquadramento WHERE id_enquadramento= :idEnquadramento'
      );

      $stmt->bindValue(':idEnquadramento', $idEnquadramento);
      $stmt->execute();

      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM enquadramento'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $enquadramento = new EnquadramentoModel();

        $enquadramento->setIdEnquadramento($row->id_enquadramento);
        $enquadramento->setEnquadramentoNome($row->enquadramento_nome);

        $results[] = $enquadramento;
      }
    }

    return $results;
  }

  public function atualizar(EnquadramentoModel $enquadramento) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE enquadramento SET enquadramento_nome = :nome WHERE id_enquadramento = :id_enquadramento'
      );

      $stmt->bindValue(':nome', $enquadramento->getEnquadramentoNome());
      $stmt->bindValue(':id_enquadramento', $enquadramento->getIdEnquadramento());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idEnquadramento) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM enquadramento WHERE id_enquadramento = :idEnquadramento'
      );
      $statement->bindValue(':idEnquadramento', $idEnquadramento);
      $statement->execute();

      return $this->processaResultados($statement);

    } catch(Exception $e) {
      echo "Erro: $e";
    }

  }

  public function getByNome(string $nomeEnquadramento) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM enquadramento WHERE enquadramento_nome = :nome'
      );
      $statement->bindValue(':nome', $nomeEnquadramento);
      $statement->execute();

      return $this->processaResultados($statement);
    } catch(Exception $e) {
      echo "Erro: $e";
    }
  }

}
