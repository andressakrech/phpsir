<?php

include_once(ABSPATH.'/model/contato_operadora_model.php');

class ContatoOperadoraDao
{

  private $conn;

  public function __construct()
  {
    $this->conn = Registry::getInstance();
  }

  public function inserir(ContatoOperadoraModel $contatoOperadora)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO contato_operadora (nome, observacao, email, telefone, id_operadora) 
        VALUES (:nome, :observacao, :email, :telefone, :id_operadora)'
      );

      $stmt->bindValue(':nome', $contatoOperadora->getNome());
      $stmt->bindValue(':observacao', $contatoOperadora->getObservacao());
      $stmt->bindValue(':email', $contatoOperadora->getEmail());
      $stmt->bindValue(':telefone', $contatoOperadora->getTelefone());
      $stmt->bindValue(':id_operadora', $contatoOperadora->getIdOperadora());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }
  }

  public function excluir(int $idContatoOperadora) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM contato_operadora WHERE id_contato_operadora= :idContatoOperadora'
      );

      $stmt->bindValue(':idContatoOperadora', $idContatoOperadora);
      $stmt->execute();

      $this->conn->commit();
      return true;
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM contato_operadora'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $contatoOperadora = new ContatoOperadoraModel();

        $contatoOperadora->setIdContatoOperadora($row->id_contato_operadora);
        $contatoOperadora->setNome($row->nome);
        $contatoOperadora->setEmail($row->email);
        $contatoOperadora->setTelefone($row->telefone);
        $contatoOperadora->setObservacao($row->observacao);
        $contatoOperadora->setIdOperadora($row->id_operadora);

        $results[] = $contatoOperadora;
      }
    }

    return $results;
  }

  public function atualizar(ContatoOperadoraModel $contatoOperadora) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE contato_operadora 
        SET nome = :nome, observacao = :observacao, email = :email, telefone = :telefone, id_operadora =:id_operadora
        WHERE id_contato_operadora = :id_contato_operadora'
      );

      $stmt->bindValue(':nome', $contatoOperadora->getNome());
      $stmt->bindValue(':observacao', $contatoOperadora->getObservacao());
      $stmt->bindValue(':email', $contatoOperadora->getEmail());
      $stmt->bindValue(':telefone', $contatoOperadora->getTelefone());
      $stmt->bindValue(':id_operadora', $contatoOperadora->getIdOperadora());
      $stmt->bindValue(':id_contato_operadora', $contatoOperadora->getIdContatoOperadora());
      $stmt->execute();

      $this->conn->commit();
      return true;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idContatoOperadora) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM contato_operadora WHERE id_contato_operadora = :idContatoOperadora'
      );
      $statement->bindValue(':idContatoOperadora', $idContatoOperadora);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }

    return $this->processaResultados($statement);
  }

  public function getByNome(string $nomeContatoOperadora) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM contato_operadora WHERE contato_operadora_nome = :nome'
      );
      $statement->bindValue(':nome', $nomeContatoOperadora);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }
    return $this->processaResultados($statement);
  }

  public function getByIdOperadora(int $idOperadora) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM contato_operadora WHERE id_operadora = :idOperadora'
      );
      $statement->bindValue(':idOperadora', $idOperadora);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
      return null;
    }

    return $this->processaResultados($statement);
  }

}
