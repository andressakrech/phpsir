<?php

include_once(ABSPATH.'/model/interface_model.php');
include_once(ABSPATH.'/dao/ipv4_dao.php');
include_once(ABSPATH.'/dao/ipv6_dao.php');

class InterfaceDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(InterfaceModel $interface)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO interface (ifindex, ifalias, ifname, ifdescr, id_equipamento)
        VALUES (:ifindex, :ifalias, :ifname, :ifdescr, :id_equipamento)
        ON DUPLICATE KEY UPDATE ifindex = :ifindex, ifalias = :ifalias, ifname = :ifname, ifdescr = :ifdescr, id_equipamento = :id_equipamento'
      );

      $stmt->bindValue(':ifindex', $interface->getIfIndex());
      $stmt->bindValue(':ifalias', $interface->getIfAlias());
      $stmt->bindValue(':ifname', $interface->getIfName());
      $stmt->bindValue(':ifdescr', $interface->getIfDescr());
      $stmt->bindValue(':id_equipamento', $interface->getIdEquipamento());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();

      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idInterface) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM interface WHERE id_interface= :idInterface'
      );

      $stmt->bindValue(':idInterface', $idInterface);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
	{
    $statement = $this->conn->query(
      'SELECT * FROM interface'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
	
	{
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $interface = new InterfaceModel();

        $interface->setIdInterface($row->id_interface);
        $interface->setIfIndex($row->ifindex);
        $interface->setIfAlias($row->ifalias);
        $interface->setIfName($row->ifname);
        $interface->setIfDescr($row->ifdescr);
        $interface->setIdEquipamento($row->id_equipamento);

        $results[] = $interface;
      }
    }

    return $results;
  }

  public function atualizar(InterfaceModel $interface) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE interface 
        SET ifindex = :ifindex, ifalias = :ifalias, ifname = :ifname, ifdescr = :ifdescr, id_equipamento = :id_equipamento
        WHERE id_interface = :id_interface'
      );

      $stmt->bindValue(':ifindex', $interface->getIfIndex());
      $stmt->bindValue(':ifalias', $interface->getIfAlias());
      $stmt->bindValue(':ifname', $interface->getIfName);
      $stmt->bindValue(':ifdescr', $interface->getIfDescr);
      $stmt->bindValue(':id_equipamento', $interface->getIdEquipamento);
      $stmt->bindValue(':id_interface', $interface->getIdInterface());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idInterface) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM interface WHERE id_interface = :idInterface'
      );
      $statement->bindValue(':idInterface', $idInterface);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByIdEquipamento(int $idEquipamento) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM interface WHERE id_equipamento = :idEquipamento'
      );
      $statement->bindValue(':idEquipamento', $idEquipamento);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

}
