<?php

include_once(ABSPATH.'/model/instituicao_model.php');

class InstituicaoDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(InstituicaoModel $instituicao)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO instituicao (instituicao_nome, instituicao_sigla, instituicao_site, id_enquadramento, reitoria)
        VALUES (:nome, :instituicao_sigla, :instituicao_site, :id_enquadramento, :reitoria)'
      );

      $stmt->bindValue(':nome', $instituicao->getInstituicaoNome());
      $stmt->bindValue(':instituicao_sigla', $instituicao->getInstituicaoSigla());
      $stmt->bindValue(':instituicao_site', $instituicao->getInstituicaoSite());
      $stmt->bindValue(':id_enquadramento', $instituicao->getIdEnquadramento());
      $stmt->bindValue(':reitoria', $instituicao->getReitoria());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      $this->erroDb($e);
      return null;
    }

  }

  private function erroDb(Exception $e) {
    if (DEBUG) {
        echo "Erro: $e";
      }
  }

  public function excluir(int $idInstituicao) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM instituicao WHERE id_instituicao = :idInstituicao'
      );

      $stmt->bindValue(':idInstituicao', $idInstituicao);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      $this->erroDb($e);
    }
    return true;
  }



  public function listar() 
	{
    $statement = $this->conn->query(
      'SELECT * FROM instituicao ORDER BY instituicao_sigla, instituicao_nome'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
	
	{
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $instituicao = new InstituicaoModel();

        $instituicao->setIdInstituicao($row->id_instituicao);
        $instituicao->setInstituicaoNome($row->instituicao_nome);
        $instituicao->setInstituicaoSigla($row->instituicao_sigla);
        $instituicao->setInstituicaoSite($row->instituicao_site);
        $instituicao->setIdEnquadramento($row->id_enquadramento);
        $instituicao->setReitoria($row->reitoria);

        $results[] = $instituicao;
      }
    }

    return $results;
  }

  public function atualizar(InstituicaoModel $instituicao) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE instituicao
        SET instituicao_nome = :nome, instituicao_sigla = :instituicao_sigla, instituicao_site = :instituicao_site,
        id_enquadramento = :id_enquadramento, reitoria = :reitoria
        WHERE id_instituicao = :id_instituicao'
      );

      $stmt->bindValue(':nome', $instituicao->getInstituicaoNome());
      $stmt->bindValue(':instituicao_sigla', $instituicao->getInstituicaoSigla());
      $stmt->bindValue(':instituicao_site', $instituicao->getInstituicaoSite());
      $stmt->bindValue(':id_enquadramento', $instituicao->getIdEnquadramento());
      $stmt->bindValue(':reitoria', $instituicao->getReitoria());
      $stmt->bindValue(':id_instituicao', $instituicao->getIdInstituicao());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      $this->erroDb($e);
    }
    return true;
  }

  public function getById(int $idInstituicao) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM instituicao WHERE id_instituicao = :idInstituicao'
      );
      $statement->bindValue(':idInstituicao', $idInstituicao);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getBySigla(string $instituicaoSigla) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM instituicao WHERE instituicao_sigla = :instituicao_sigla'
      );
      $statement->bindValue(':instituicao_sigla', $instituicaoSigla);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

  public function getByEnquadramento(int $idEnquadramento) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM instituicao WHERE id_enquadramento = :id_enquadramento'
      );
      $statement->bindValue(':id_enquadramento', $idEnquadramento);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

    public function getLikeSigla(string $instituicaoSigla) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM instituicao WHERE instituicao_sigla LIKE :instituicao_sigla'
      );
      $statement->bindValue(':instituicao_sigla', "%{$instituicaoSigla}%");
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
