<?php

include_once(ABSPATH.'/model/email_contato_operadora_model.php');

class EmailContatoOperadoraDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(EmailContatoOperadoraModel $emailContatoOperadora)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO email_contato_operadora (email, id_contato_operadora) VALUES (:email, :id_contato_operadora)'
      );

      $stmt->bindValue(':email', $emailContatoOperadora->getEmail());
      $stmt->bindValue(':id_contato_operadora', $emailContatoOperadora->getIdContatoOperadora());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idEmailContatoOperadora) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM email_contato_operadora WHERE id_email_contato_operadora= :id_email_contato_operadora'
      );

      $stmt->bindValue(':id_email_contato_operadora', $idEmailContatoOperadora);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM email_contato_operadora'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $email_contato_operadora = new Email_contato_operadoraModel();

        $email_contato_operadora->setIdEmailContatoOperadora($row->id_email_contato_operadora);
        $email_contato_operadora->setEmail($row->email);
        $email_contato_operadora->setIdContatoOperadora($row->id_contato_operadora);

        $results[] = $email_contato_operadora;
      }
    }

    return $results;
  }

  public function atualizar(EmailContatoOperadoraModel $emailContatoOperadora) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE email_contato_operadora 
        SET email = :email, id_contato_operadora = :id_contato_operadora
        WHERE id_email_contato_operadora = :id_email_contato_operadora'
      );

      $stmt->bindValue(':email', $emailContatoOperadora->getEmail());
      $stmt->bindValue(':id_contato_operadora', $emailContatoOperadora->getIdContatoOperadora());
      $stmt->bindValue(':id_email_contato_operadora', $emailContatoOperadora->getIdEmailContatoOperadora());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idEmailContatoOperadora) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM email_contato_operadora WHERE id_email_contato_operadora = :id_email_contato_operadora'
      );
      $statement->bindValue(':id_email_contato_operadora', $idEmailContatoOperadora);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByIdContatoOperadora(int $idContatoOperadora) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM email_contato_operadora WHERE id_contato_operadora = :id_contato_operadora'
      );
      $statement->bindValue(':id_contato_operadora', $idContatoOperadora);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
