<?php

include_once(ABSPATH.'/model/email_contato_unidade_model.php');

class EmailContatoUnidadeDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(EmailContatoUnidadeModel $emailContatoUnidade)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO email_contato_unidade (email, id_contato_unidade) VALUES (:email, :id_contato_unidade)'
      );

      $stmt->bindValue(':email', $emailContatoUnidade->getEmail());
      $stmt->bindValue(':id_contato_unidade', $emailContatoUnidade->getIdContatoUnidade());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idEmailContatoUnidade) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM email_contato_unidade WHERE id_email_contato_unidade= :id_email_contato_unidade'
      );

      $stmt->bindValue(':id_email_contato_unidade', $idEmailContatoUnidade);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM email_contato_unidade'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $email_contato_unidade = new Email_contato_unidadeModel();

        $email_contato_unidade->setIdEmailContatoUnidade($row->id_email_contato_unidade);
        $email_contato_unidade->setEmail($row->email);
        $email_contato_unidade->setIdContatoUnidade($row->id_contato_unidade);

        $results[] = $email_contato_unidade;
      }
    }

    return $results;
  }

  public function atualizar(EmailContatoUnidadeModel $emailContatoUnidade) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE email_contato_unidade 
        SET email = :email, id_contato_unidade = :id_contato_unidade
        WHERE id_email_contato_unidade = :id_email_contato_unidade'
      );

      $stmt->bindValue(':email', $emailContatoUnidade->getEmail());
      $stmt->bindValue(':id_contato_unidade', $emailContatoUnidade->getIdContatoUnidade());
      $stmt->bindValue(':id_email_contato_unidade', $emailContatoUnidade->getIdEmailContatoUnidade());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idEmailContatoUnidade) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM email_contato_unidade WHERE id_email_contato_unidade = :id_email_contato_unidade'
      );
      $statement->bindValue(':id_email_contato_unidade', $idEmailContatoUnidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByIdContatoUnidade(int $idContatoUnidade) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM email_contato_unidade WHERE id_contato_unidade = :id_contato_unidade'
      );
      $statement->bindValue(':id_contato_unidade', $idContatoUnidade);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
