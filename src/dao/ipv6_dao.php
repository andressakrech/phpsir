<?php

include_once(ABSPATH.'/model/ipv6_model.php');

class Ipv6Dao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(Ipv6Model $ipv6)
	{
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO ipv6 (endereco_ipv6, id_interface) VALUES (:enderecoIpv6, :idInterface)'
      );

      $stmt->bindValue(':enderecoIpv6', $ipv6->getEnderecoIpv6());
      $stmt->bindValue(':idInterface', $ipv6->getIdInterface());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idIpv6) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM ipv6 WHERE id_ipv6= :idIpv6'
      );

      $stmt->bindValue(':idIpv6', $idIpv6);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM ipv6'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $ipv6 = new Ipv6Model();

        $ipv6->setIdIpv6($row->id_ipv6);
        $ipv6->setEnquadramentoNome($row->endereco_ipv6);
        $ipv6->setIdInterface($row->id_interface);

        $results[] = $ipv6;
      }
    }

    return $results;
  }

  public function atualizar(Ipv6Model $ipv6) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE ipv6
        SET endereco_ipv6 = :nome, id_interface = :idInterface
        WHERE id_ipv6 = :id_ipv6'
      );

      $stmt->bindValue(':nome', $ipv6->getEnderecoIpv6());
      $stmt->bindValue(':id_interface', $ipv6->getIdInterface());
      $stmt->bindValue(':id_ipv6', $ipv6->getIdIpv6());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idIpv6) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM ipv6 WHERE id_ipv6 = :idIpv6'
      );
      $statement->bindValue(':idIpv6', $idIpv6);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByIdInterface(int $idInterface) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM ipv6 WHERE id_interface = :idInterface'
      );
      $statement->bindValue(':idInterface', $idInterface);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

  public function getByEnderecoIpv6(string $endereco) {
    try 
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM ipv6 WHERE endereco_ipv6 = :endereco'
      );
      $statement->bindValue(':endereco', $endereco);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }
    return $this->processaResultados($statement);
  }

}
