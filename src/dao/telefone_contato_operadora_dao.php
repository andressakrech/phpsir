<?php

include_once(ABSPATH.'/model/telefone_contato_operadora_model.php');

class TelefoneContatoOperadoraDao
{

  private $conn;

  public function __construct()
	{
    $this->conn = Registry::getInstance();
  }

  public function inserir(TelefoneContatoOperadoraModel $telefoneContatoOperadora)
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'INSERT INTO telefone_contato_operadora (ddd, numero, id_contato_operadora) 
        VALUES (:ddd, :numero, :id_contato_operadora)'
      );

      $stmt->bindValue(':ddd', $telefoneContatoOperadora->getDdd());
      $stmt->bindValue(':numero', $telefoneContatoOperadora->getNumero());
      $stmt->bindValue(':id_contato_operadora', $telefoneContatoOperadora->getIdContatoOperadora());
      $stmt->execute();

      $id = $this->conn->lastInsertId();
      $this->conn->commit();
      return $id;
    }
    catch(Exception $e) {
      $this->conn->rollback();
      if (DEBUG) {
        echo "Erro: $e";
      }
      return null;
    }

  }

  public function excluir(int $idTelefoneContatoOperadora) 
  {
    $this->conn->beginTransaction();

    try {
      $stmt = $this->conn->prepare(
        'DELETE FROM telefone_contato_operadora WHERE id_telefone_contato_operadora = :idTelefoneContatoOperadora'
      );

      $stmt->bindValue(':idTelefoneContatoOperadora', $idTelefoneContatoOperadora);
      $stmt->execute();

      $this->conn->commit();
    } catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }



  public function listar() 
  {
    $statement = $this->conn->query(
      'SELECT * FROM telefone_contato_operadora'
    );

    return $this->processaResultados($statement);
  }


  private function processaResultados($statement) 
  {
    $results = array();

    if($statement) {
      while($row = $statement->fetch(PDO::FETCH_OBJ)) {
        $telefoneContatoOperadora = new TelefoneContatoOperadoraModel();

        $telefoneContatoOperadora->setIdTelefoneContatoOperadora($row->id_telefone_contato_operadora);
        $telefoneContatoOperadora->setDdd($row->ddd);
        $telefoneContatoOperadora->setNumero($row->numero);
        $telefoneContatoOperadora->setIdContatoOperadora($row->id_contato_operadora);

        $results[] = $telefoneContatoOperadora;
      }
    }

    return $results;
  }

  public function atualizar(TelefoneContatoOperadoraModel $telefone_contato_operadora) {
    $this->conn->beginTransaction();
    try {
      $stmt = $this->conn->prepare(
        'UPDATE telefone_contato_operadora 
        SET ddd = :ddd, numero = :numero, id_contato_operadora = :id_contato_operadora
        WHERE id_telefone_contato_operadora = :id_telefone_contato_operadora'
      );

      $stmt->bindValue(':ddd', $telefone_contato_operadora->getDdd());
      $stmt->bindValue(':numero', $telefone_contato_operadora->getNumero());
      $stmt->bindValue(':id_contato_operadora', $telefone_contato_operadora->getIdContatoOperadora());
      $stmt->bindValue(':id_telefone_contato_operadora', $telefone_contato_operadora->getIdTelefoneContatoOperadora());
      $stmt->execute();

      $this->conn->commit();
    }
    catch(Exception $e) {
      $this->conn->rollback();
      echo "Erro: $e";
    }
  }

  public function getById(int $idTelefoneContatoOperadora) {
    try
    {
      $statement = $this->conn->prepare(
        'SELECT * FROM telefone_contato_operadora WHERE id_telefone_contato_operadora = :idTelefoneContatoOperadora'
      );
      $statement->bindValue(':idTelefoneContatoOperadora', $idTelefoneContatoOperadora);
      $statement->execute();

    } catch(Exception $e) {
      echo "Erro: $e";
    }

    return $this->processaResultados($statement);
  }

}
