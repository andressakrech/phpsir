<?php

class Ipv4Model
{

  private $idIpv4;
  private $enderecoIpv4;
  private $idInterface;

  public function __construct($endereco, $idInterface) {
    $this->enderecoIpv4 = empty($endereco) ? "" : $endereco;
    $this->idInterface = $idInterface;
  }

  public function toArray()
  {
    return array('id_ipv4' => $this->idIpv4,
    'endereco_ipv4' => $this->enderecoIpv4,
    'id_interface' => $this->idInterface);
  }


    public function getIdIpv4() : int
    {
        return $this->idIpv4;
    }

    public function setIdIpv4(int $idIpv4)
    {
        $this->idIpv4 = $idIpv4;

        return $this;
    }

    public function getEnderecoIpv4() : string
    {
        return $this->enderecoIpv4;
    }

    public function setEnderecoIpv4(string $enderecoIpv4)
    {
        if (filter_var($enderecoIpv4, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
        {
          $this->enderecoIpv4 = $enderecoIpv4;
        } 
        return $this;
    }

    public function getIdInterface() : int
    {
        return $this->idInterface;
    }

    public function setIdInterface(int $idInterface)
    {
        $this->idInterface = $idInterface;

        return $this;
    }

}
