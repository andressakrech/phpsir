<?php

class OperadoraModel {

  private $idOperadora;
  private $operadoraNome;
  private $operadoraSigla;
  private $site;
  private $observacao;

  public function toArray()
  {
    return array(
      'id_operadora' => $this->idOperadora,
      'operadora_nome' => $this->operadoraNome,
      'operadora_sigla' => $this->operadoraSigla,
      'site' => $this->site,
      'observacao' => $this->observacao);
  }

    /**
     * Get the value of Id Operadora
     *
     * @return mixed
     */
    public function getIdOperadora() : int
    {
        return $this->idOperadora;
    }

    /**
     * Set the value of Id Operadora
     *
     * @param mixed idOperadora
     *
     * @return self
     */
    public function setIdOperadora(int $idOperadora)
    {
        $this->idOperadora = $idOperadora;

        return $this;
    }

    /**
     * Get the value of Operadora Nome
     *
     * @return mixed
     */
    public function getOperadoraNome() : string
    {
        return $this->operadoraNome;
    }

    /**
     * Set the value of Operadora Nome
     *
     * @param mixed operadoraNome
     *
     * @return self
     */
    public function setOperadoraNome(string $operadoraNome)
    {
        $this->operadoraNome = $operadoraNome;

        return $this;
    }

    /**
     * Get the value of Operadora Sigla
     *
     * @return mixed
     */
    public function getOperadoraSigla() : string
    {
        return $this->operadoraSigla;
    }

    /**
     * Set the value of Operadora Sigla
     *
     * @param mixed operadoraSigla
     *
     * @return self
     */
    public function setOperadoraSigla(string $operadoraSigla)
    {
        $this->operadoraSigla = $operadoraSigla;

        return $this;
    }

    /**
     * Get the value of Site
     *
     * @return mixed
     */
    public function getSite() : string
    {
        return $this->site;
    }

    /**
     * Set the value of Site
     *
     * @param mixed site
     *
     * @return self
     */
    public function setSite(string $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get the value of Observacao
     *
     * @return mixed
     */
    public function getObservacao() : string
    {
        return $this->observacao;
    }

    /**
     * Set the value of Observacao
     *
     * @param mixed observacao
     *
     * @return self
     */
    public function setObservacao(string $observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

}
