<?php

  class TelefoneContatoUnidadeModel
  {

    private $idTelefoneContatoUnidade;
    private $ddd;
    private $numero;
    private $idContatoUnidade;

    public function toArray()
    {
      return array(
        'id_telefone_contato_unidade' => $this->idTelefoneContatoUnidade,
        'ddd' => $this->ddd,
        'numero' => $this->numero,
        'id_contato_unidade' => $this->idContatoUnidade
      );
    }

    /**
     * Get the value of Id Telefone Contato Unidade
     *
     * @return mixed
     */
    public function getIdTelefoneContatoUnidade() : int
    {
        return $this->idTelefoneContatoUnidade;
    }

    /**
     * Set the value of Id Telefone Contato Unidade
     *
     * @param mixed idTelefoneContatoUnidade
     *
     * @return self
     */
    public function setIdTelefoneContatoUnidade(int $idTelefoneContatoUnidade)
    {
        $this->idTelefoneContatoUnidade = $idTelefoneContatoUnidade;

        return $this;
    }

    /**
     * Get the value of Ddd
     *
     * @return mixed
     */
    public function getDdd()
    {
        return $this->ddd;
    }

    /**
     * Set the value of Ddd
     *
     * @param mixed ddd
     *
     * @return self
     */
    public function setDdd($ddd)
    {
        $this->ddd = $ddd;

        return $this;
    }

    /**
     * Get the value of Numero
     *
     * @return mixed
     */
    public function getNumero() : string
    {
        return $this->numero;
    }

    /**
     * Set the value of Numero
     *
     * @param mixed numero
     *
     * @return self
     */
    public function setNumero(string $numero)
    {
        // adiciona o numero com ddd informado se estiver correto
        preg_match('/^(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/', $numero) ? $this->numero = $numero : "";

        return $this;
    }

    /**
     * Get the value of Id Contato Unidade
     *
     * @return mixed
     */
    public function getIdContatoUnidade() : int
    {
        return $this->idContatoUnidade;
    }

    /**
     * Set the value of Id Contato Unidade
     *
     * @param mixed idContatoUnidade
     *
     * @return self
     */
    public function setIdContatoUnidade(int $idContatoUnidade)
    {
        $this->idContatoUnidade = $idContatoUnidade;

        return $this;
    }

}
