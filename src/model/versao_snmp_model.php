<?php

class VersaoSnmpModel {

  private $idVersaoSnmp;//int
  private $versaoSnmp;//string

  public function toArray()
  {
    return array(
    'id_versao_snmp' => $this->idVersaoSnmp,
    'versao_snmp' => $this->versaoSnmp
    );
  }

    /**
     * Get the value of Id Versao Snmp
     *
     * @return mixed
     */
    public function getIdVersaoSnmp() : int
    {
        return $this->idVersaoSnmp;
    }

    /**
     * Set the value of Id Versao Snmp
     *
     * @param mixed idVersaoSnmp
     *
     * @return self
     */
    public function setIdVersaoSnmp(int $idVersaoSnmp)
    {
        $this->idVersaoSnmp = $idVersaoSnmp;

        return $this;
    }

    /**
     * Get the value of Versao Snmp
     *
     * @return mixed
     */
    public function getVersaoSnmp() : string
    {
        return $this->versaoSnmp;
    }

    /**
     * Set the value of Versao Snmp
     *
     * @param mixed versaoSnmp
     *
     * @return self
     */
    public function setVersaoSnmp(string $versaoSnmp)
    {
        $this->versaoSnmp = $versaoSnmp;

        return $this;
    }

}
