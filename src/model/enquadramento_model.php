<?php

class EnquadramentoModel {

  private $idEnquadramento;//int
  private $enquadramentoNome;//string

  public function getIdEnquadramento() : int {
    return $this->idEnquadramento;
  }

  public function setIdEnquadramento(int $idEnquadramento) {
    $this->idEnquadramento = $idEnquadramento;
  }

  public function getEnquadramentoNome() : string {
    return $this->enquadramentoNome;
  }

  public function setEnquadramentoNome(string $enquadramentoNome) {
    $this->enquadramentoNome = $enquadramentoNome;
  }

  public function toArray() {
    return array(  'id_enquadramento' => $this->idEnquadramento,
        'enquadramento_nome' => $this->enquadramentoNome);
  }

}
