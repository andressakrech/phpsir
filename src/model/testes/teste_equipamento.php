<?php

include ('../equipamento_model.php');

$equipamento = new EquipamentoModel();

$equipamento->setIdEquipamento(1);
$equipamento->setHostname('teste.ger.tche.br');
$equipamento->setIpv4('10.1.0.2');
$equipamento->setIpv6('2804:ba1a::cafe');
$equipamento->setFabricante('cisco');
$equipamento->setModelo('c12k');
$equipamento->setComunidadeSnmp('comunidade');
$equipamento->setIdVersaoSnmp('1');

echo $equipamento->getIdEquipamento()."\r\n";
echo $equipamento->getHostname()."\r\n";
echo $equipamento->getIpv4()."\r\n";
echo $equipamento->getIpv6()."\r\n";
echo $equipamento->getFabricante()."\r\n";
echo $equipamento->getModelo()."\r\n";
echo $equipamento->getComunidadeSnmp()."\r\n";
echo $equipamento->getIdVersaoSnmp()."\r\n";

PRINT_R($equipamento->toArray());
