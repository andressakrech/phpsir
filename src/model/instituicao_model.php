<?php

class InstituicaoModel {

  private $idInstituicao;
  private $instituicaoNome;
  private $instituicaoSigla;
  private $instituicaoSite;
  private $idEnquadramento;
  private $reitoria;

    /**
     * Get the value of Id Instituicao
     *
     * @return mixed
     */
    public function getIdInstituicao() : int
    {
        return $this->idInstituicao;
    }

    /**
     * Set the value of Id Instituicao
     *
     * @param mixed idInstituicao
     *
     * @return self
     */
    public function setIdInstituicao(int $idInstituicao)
    {
        $this->idInstituicao = $idInstituicao;

        return $this;
    }

    /**
     * Get the value of Instituicao Nome
     *
     * @return mixed
     */
    public function getInstituicaoNome() : string
    {
        return $this->instituicaoNome;
    }

    /**
     * Set the value of Instituicao Nome
     *
     * @param mixed instituicaoNome
     *
     * @return self
     */
    public function setInstituicaoNome(string $instituicaoNome)
    {
        $this->instituicaoNome = $instituicaoNome;

        return $this;
    }

    /**
     * Get the value of Instituicao Sigla
     *
     * @return mixed
     */
    public function getInstituicaoSigla() : string
    {
        return $this->instituicaoSigla;
    }

    /**
     * Set the value of Instituicao Sigla
     *
     * @param mixed instituicaoSigla
     *
     * @return self
     */
    public function setInstituicaoSigla(string $instituicaoSigla)
    {
        $this->instituicaoSigla = $instituicaoSigla;

        return $this;
    }

    /**
     * Get the value of Instituicao Site
     *
     * @return mixed
     */
    public function getInstituicaoSite() : string
    {
        return $this->instituicaoSite;
    }

    /**
     * Set the value of Instituicao Site
     *
     * @param mixed instituicaoSite
     *
     * @return self
     */
    public function setInstituicaoSite(string $instituicaoSite)
    {
        $this->instituicaoSite = $instituicaoSite;

        return $this;
    }

    /**
     * Get the value of Id Enquadramento
     *
     * @return mixed
     */
    public function getIdEnquadramento() : int
    {
        return $this->idEnquadramento;
    }

    /**
     * Set the value of Id Enquadramento
     *
     * @param mixed idEnquadramento
     *
     * @return self
     */
    public function setIdEnquadramento(int $idEnquadramento)
    {
        $this->idEnquadramento = $idEnquadramento;

        return $this;
    }

    /**
     * Get the value of Reitoria
     *
     * @return mixed
     */
    public function getReitoria()//tipo : int ou null
    {
        return $this->reitoria;
    }

    /**
     * Set the value of Reitoria
     *
     * @param mixed reitoria
     *
     * @return self
     */
    public function setReitoria($reitoria)//tipo int ou null
    {
        $this->reitoria = $reitoria;

        return $this;
    }

    public function toArray()
    {
      return array(  'id_instituicao' => $this->idInstituicao,
          'instituicao_nome' => $this->instituicaoNome,
          'instituicao_sigla' => $this->instituicaoSigla,
          'instituicao_site' => $this->instituicaoSite,
          'id_enquadramento' => $this->idEnquadramento,
          'reitoria' => $this->reitoria);
    }

}
