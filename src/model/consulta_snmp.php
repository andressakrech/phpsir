<?php

class ConsultaSnmp {

	private $host;
	private $community;
	private $version;

	public function __construct($host, $community, $version='2c') {
		$this->host = $host;
		$this->community = $community;
	}

	public function toString() {
		return "host: ".$this->host."<br/>community: ".$this->community."<br/>version: ".$this->version;
	}

	//consulta snmp para buscar o indice das interfaces do equipamento
	private function discoverIfIndex() {
		//RFC 2863
		return snmp2_walk($this->host, $this->community, '1.3.6.1.2.1.2.2.1.1');
	}

	//consulta snmp para buscar o nome das interfaces do equipamento
	private function discoverIfName() {
		//RFC 2863
		return snmp2_walk($this->host, $this->community, '1.3.6.1.2.1.31.1.1.1.1');
	}
	
	//consulta snmp para buscar a descricao das interfaces do equipamento
	private function discoverIfDescr() {
		//RFC 2863
		return snmp2_walk($this->host, $this->community,'1.3.6.1.2.1.2.2.1.2');
	}

	//consulta snmp para buscar o apelido das interfaces do equipamento
	private function discoverIfAlias() {
		//RFC 2863
		return snmp2_walk($this->host, $this->community, '1.3.6.1.2.1.31.1.1.1.18');
	}
	
	//realiza as buscas snmp no equipamento e retorna um array com as informacoes
	public function getEquipamentInformation() {
		$ifIndex = $this->discoverIfIndex();
		$ifName = $this->discoverIfName();
		$ifDescr = $this->discoverIfDescr();
		$ifAlias = $this->discoverIfAlias();

		foreach($ifIndex as $key=>$value) {
			$EqtoInfo[$key]['ifIndex'] = str_replace('INTEGER: ', '', $value);//$value = $ifIndex[$key]
			$EqtoInfo[$key]['ifName'] = str_replace('STRING: ', '', $ifName[$key]);
			$EqtoInfo[$key]['ifDescr'] = str_replace('STRING: ', '', $ifDescr[$key]);
			$EqtoInfo[$key]['ifAlias'] = str_replace('STRING: ', '', $ifAlias[$key]);
		}
		return $EqtoInfo;
	}

	public function getHost() {
		return $this->host;

	}

}


