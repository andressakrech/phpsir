<?php

include_once(ABSPATH.'/model/endereco_model.php');

class UnidadeModel {

  private $idUnidade;
  private $unidadeNome;
  private $unidadeSigla;
  private $dataConexao;
  private $observacao;
  private $idInstituicao;
  private $endereco;

  public function toArray() {
    return array(
      'id_unidade' => $this->idUnidade,
      'unidade_nome' => $this->unidadeNome,
      'unidade_sigla' => $this->unidadeSigla,
      'data_conexao' => $this->dataConexao,
      'observacao' => $this->observacao,
      'id_instituicao' => $this->idInstituicao,
      'endereco' => $this->endereco->toArray()
    );
  }

    /**
     * Get the value of Id Unidade
     *
     * @return mixed
     */
    public function getIdUnidade() : int
    {
        return $this->idUnidade;
    }

    /**
     * Set the value of Id Unidade
     *
     * @param mixed idUnidade
     *
     * @return self
     */
    public function setIdUnidade(int $idUnidade)
    {
        $this->idUnidade = $idUnidade;

        return $this;
    }

    /**
     * Get the value of Unidade Nome
     *
     * @return mixed
     */
    public function getUnidadeNome() : string
    {
        return $this->unidadeNome;
    }

    /**
     * Set the value of Unidade Nome
     *
     * @param mixed unidadeNome
     *
     * @return self
     */
    public function setUnidadeNome(string $unidadeNome)
    {
        $this->unidadeNome = $unidadeNome;

        return $this;
    }

    /**
     * Get the value of Unidade Sigla
     *
     * @return mixed
     */
    public function getUnidadeSigla() : string
    {
        return $this->unidadeSigla;
    }

    /**
     * Set the value of Unidade Sigla
     *
     * @param mixed unidadeSigla
     *
     * @return self
     */
    public function setUnidadeSigla(string $unidadeSigla)
    {
        $this->unidadeSigla = $unidadeSigla;

        return $this;
    }

    /**
     * Get the value of Data Conexao
     *
     * @return mixed
     */
    public function getDataConexao() : string
    {
        return $this->dataConexao;
    }

    /**
     * Set the value of Data Conexao
     *
     * @param mixed dataConexao
     *
     * @return self
     */
    public function setDataConexao(string $dataConexao)
    {
        $this->dataConexao = $dataConexao;

        return $this;
    }

    /**
     * Get the value of Observacao
     *
     * @return mixed
     */
    public function getObservacao() : string
    {
        return $this->observacao;
    }

    /**
     * Set the value of Observacao
     *
     * @param mixed observacao
     *
     * @return self
     */
    public function setObservacao(string $observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get the value of Id Instituicao
     *
     * @return mixed
     */
    public function getIdInstituicao() : int
    {
        return $this->idInstituicao;
    }

    /**
     * Set the value of Id Instituicao
     *
     * @param mixed idInstituicao
     *
     * @return self
     */
    public function setIdInstituicao(int $idInstituicao)
    {
        $this->idInstituicao = $idInstituicao;

        return $this;
    }

    /**
     * Get the value of Id Endereco
     *
     * @return mixed
     */
    public function getEndereco() : EnderecoModel
    {
        return $this->endereco;
    }

    /**
     * Set the value of Id Endereco
     *
     * @param mixed idEndereco
     *
     * @return self
     */
    public function setEndereco(EnderecoModel $endereco)
    {
        $this->endereco = $endereco;

        return $this;
    }

}
