<?php

class ConexaoModel
{

  private $idConexao;
  private $conexao;

  public function toArray()
  {
    return array(  'id_conexao' => $this->idConexao,
        'conexao' => $this->conexao);
  }

    /**
     * Get the value of Id Conexao
     *
     * @return mixed
     */
    public function getIdConexao() : int
    {
        return $this->idConexao;
    }

    /**
     * Set the value of Id Conexao
     *
     * @param mixed idConexao
     *
     * @return self
     */
    public function setIdConexao(int $idConexao)
    {
        $this->idConexao = $idConexao;

        return $this;
    }

    /**
     * Get the value of Conexao
     *
     * @return mixed
     */
    public function getConexao() : string
    {
        return $this->conexao;
    }

    /**
     * Set the value of Conexao
     *
     * @param mixed conexao
     *
     * @return self
     */
    public function setConexao(string $conexao)
    {
        $this->conexao = $conexao;

        return $this;
    }

}
