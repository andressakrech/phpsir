<?php
include_once(ABSPATH.'/functions/valida_telefone.php');
class ContatoUnidadeModel
{

  private $idContatoUnidade;
  private $nome;
  private $observacao;
  private $tecnico;
  private $seguranca;
  private $administrativo;
  private $email;
  private $telefone;
  private $idUnidade;

  public function toArray()
  {

    return array(
      'id_contato_unidade' => $this->idContatoUnidade,
      'nome' => $this->nome,
      'observacao' => $this->observacao,
      'tecnico' => $this->tecnico,
      'seguranca' => $this->seguranca,
      'administrativo' => $this->administrativo,
      'email' => $this->email,
      'telefone' => $this->telefone,
      'id_unidade' => $this->idUnidade
    );
  }


    /**
     * Get the value of Id Contato Unidade
     *
     * @return mixed
     */
    public function getIdContatoUnidade() : int
    {
        return $this->idContatoUnidade;
    }

    /**
     * Set the value of Id Contato Unidade
     *
     * @param mixed idContatoUnidade
     *
     * @return self
     */
    public function setIdContatoUnidade(int $idContatoUnidade)
    {
        $this->idContatoUnidade = $idContatoUnidade;

        return $this;
    }

    /**
     * Get the value of Nome
     *
     * @return mixed
     */
    public function getNome() : string
    {
        return $this->nome;
    }

    /**
     * Set the value of Nome
     *
     * @param mixed nome
     *
     * @return self
     */
    public function setNome(string $nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get the value of Observacao
     *
     * @return mixed
     */
    public function getObservacao() : string
    {
        return $this->observacao;
    }

    /**
     * Set the value of Observacao
     *
     * @param mixed observacao
     *
     * @return self
     */
    public function setObservacao(string $observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get the value of Tecnico
     *
     * @return mixed
     */
    public function getTecnico() : int
    {
        return $this->tecnico;
    }

    /**
     * Set the value of Tecnico
     *
     * @param mixed tecnico
     *
     * @return self
     */
    public function setTecnico(int $tecnico)
    {
        $this->tecnico = $tecnico;

        return $this;
    }

    /**
     * Get the value of Seguranca
     *
     * @return mixed
     */
    public function getSeguranca() : int
    {
        return $this->seguranca;
    }

    /**
     * Set the value of Seguranca
     *
     * @param mixed seguranca
     *
     * @return self
     */
    public function setSeguranca(int $seguranca)
    {
        $this->seguranca = $seguranca;

        return $this;
    }

    /**
     * Get the value of Administrativo
     *
     * @return mixed
     */
    public function getAdministrativo() : int
    {
        return $this->administrativo;
    }

    /**
     * Set the value of Administrativo
     *
     * @param mixed administrativo
     *
     * @return self
     */
    public function setAdministrativo(int $administrativo)
    {
        $this->administrativo = $administrativo;

        return $this;
    }

    /**
     * Get the value of Id Unidade
     *
     * @return mixed
     */
    public function getIdUnidade() : int
    {
        return $this->idUnidade;
    }

    /**
     * Set the value of Id Unidade
     *
     * @param mixed idUnidade
     *
     * @return self
     */
    public function setIdUnidade(int $idUnidade)
    {
        $this->idUnidade = $idUnidade;

        return $this;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    public function getTelefone() : string
    {
        return $this->telefone;
    }

    public function setTelefone(string $telefone)
    {
        // se o telefone for valido adiciona
        $this->telefone = validaTelefone($telefone);
        return $this;

    }

}
