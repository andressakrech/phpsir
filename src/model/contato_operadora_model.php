<?php
include_once(ABSPATH.'/functions/valida_telefone.php');

class ContatoOperadoraModel
{

  private $idContatoOperadora;
  private $nome;
  private $observacao;
  private $email;
  private $telefone;
  private $idOperadora;

  public function toArray()
  {
    return array(
      'id_contato_operadora' => $this->idContatoOperadora,
      'nome' => $this->nome,
      'observacao' => $this->observacao,
      'email' => $this->email,
      'telefone' => $this->telefone,
      'id_operadora' => $this->idOperadora
    );
  }

    /**
     * Get the value of Id Contato Operadora
     *
     * @return mixed
     */
    public function getIdContatoOperadora() : int
    {
        return $this->idContatoOperadora;
    }

    /**
     * Set the value of Id Contato Operadora
     *
     * @param mixed idContatoOperadora
     *
     * @return self
     */
    public function setIdContatoOperadora(int $idContatoOperadora)
    {
        $this->idContatoOperadora = $idContatoOperadora;

        return $this;
    }

    /**
     * Get the value of Nome
     *
     * @return mixed
     */
    public function getNome() : string
    {
        return $this->nome;
    }

    /**
     * Set the value of Nome
     *
     * @param mixed nome
     *
     * @return self
     */
    public function setNome(string $nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get the value of Observacao
     *
     * @return mixed
     */
    public function getObservacao() : string
    {
        return $this->observacao;
    }

    /**
     * Set the value of Observacao
     *
     * @param mixed observacao
     *
     * @return self
     */
    public function setObservacao(string $observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * Set the value of Nome
     *
     * @param mixed nome
     *
     * @return self
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    public function getTelefone() : string
    {
        return $this->telefone;
    }

    /**
     * Set the value of Nome
     *
     * @param mixed nome
     *
     * @return self
     */
    public function setTelefone(string $telefone)
    {
        $this->telefone = validaTelefone($telefone);

        return $this;
    }

    /**
     * Get the value of Id Operadora
     *
     * @return mixed
     */
    public function getIdOperadora() : int
    {
        return $this->idOperadora;
    }

    /**
     * Set the value of Id Operadora
     *
     * @param mixed idOperadora
     *
     * @return self
     */
    public function setIdOperadora(int $idOperadora)
    {
        $this->idOperadora = $idOperadora;

        return $this;
    }

}
