<?php

class InterfaceModel {

  private $idInterface;
  private $ifIndex;
  private $ifAlias;
  private $ifName;
  private $ifDescr;
  private $idEquipamento;
  private $arrayIpv4;
  private $arrayIpv6;

  function __construct($ifIndex = null, $ifAlias = "", $ifName = "", 
  $ifDescr = "", $idEquipamento = null, $arrayIpv4 = array(), $arrayIpv6 = array())
  {
    $this->ifIndex = $ifIndex;
    $this->ifAlias = $ifAlias;
    $this->ifName = $ifName;
    $this->ifDescr = $ifDescr;
    $this->idEquipamento = $idEquipamento;
    $this->arrayIpv4 = $arrayIpv4;
    $this->arrayIpv6 = $arrayIpv6;
  }

  public function toArray() {

    return array(
    'id_interface' => $this->idInterface,
    'ifindex' => $this->ifIndex,
    'ifalias' => $this->ifAlias,
    'ifname' => $this->ifName,
    'ifdescr' => $this->ifDescr,
    'id_equipamento' => $this->idEquipamento,
    'array_ipv4' => $this->arrayIpv4,
    'array_ipv6' => $this->arrayIpv6
    );
  }

  public function getIdInterface()
  {
      return $this->idInterface;
  }
 
  public function setIdInterface($idInterface)
  {
    $this->idInterface = $idInterface;
    return $this;
  }
 
  public function getIfIndex()
  {
    return $this->ifIndex;
  }
 
  public function setIfIndex($ifIndex)
  {
    $this->ifIndex = $ifIndex;
    return $this;
  }
 
  public function getIfAlias()
  {
    return $this->ifAlias;
  }
 
  public function setIfAlias($ifAlias)
  {
    $this->ifAlias = $ifAlias;
 
    return $this;
  }
 
  public function getIfName()
  {
    return $this->ifName;
  }
 
  public function setIfName($ifName)
  {
    $this->ifName = $ifName;
 
    return $this;
  }
 
  public function getIfDescr()
  {
    return $this->ifDescr;
  }
 
  public function setIfDescr($ifDescr)
  {
    $this->ifDescr = $ifDescr;
 
    return $this;
  }
 
  public function getIdEquipamento()
  {
    return $this->idEquipamento;
  }
 
  public function setIdEquipamento($idEquipamento)
  {
    $this->idEquipamento = $idEquipamento;
 
    return $this;
  }

  public function getArrayIpv4()
  {
    return $this->arrayIpv4;
  }

  public function setArrayIpv4($arrayIpv4)
  {
    $this->arrayIpv4 = $arrayIpv4;

    return $this;
  }

  public function getArrayIpv6()
  {
    return $this->arrayIpv6;
  }

  public function setArrayIpv6($arrayIpv6)
  {
    $this->arrayIpv6 = $arrayIpv6;

    return $this;
  }

}
