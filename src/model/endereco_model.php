<?php

class EnderecoModel {

  private $idEndereco;
  private $endereco;
  private $bairro;
  private $cidade;
  private $cep;
  private $coordenadas;

  public function toArray() {
    return array(  'id_endereco' => $this->idEndereco,
        'endereco' => $this->endereco,
        'bairro' => $this->bairro,
        'cidade' => $this->cidade,
        'cep' => $this->cep,
        'coordenada' => $this->coordenadas);
  }

    /**
     * Get the value of Id Endereco
     *
     * @return mixed
     */
    public function getIdEndereco() : int
    {
        return $this->idEndereco;
    }

    /**
     * Set the value of Id Endereco
     *
     * @param mixed idEndereco
     *
     * @return self
     */
    public function setIdEndereco(int $idEndereco)
    {
        $this->idEndereco = $idEndereco;

        return $this;
    }

    /**
     * Get the value of Endereco
     *
     * @return mixed
     */
    public function getEndereco() : string
    {
        return $this->endereco;
    }

    /**
     * Set the value of Endereco
     *
     * @param mixed endereco
     *
     * @return self
     */
    public function setEndereco(string $endereco)
    {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get the value of Bairro
     *
     * @return mixed
     */
    public function getBairro() : string
    {
        return $this->bairro;
    }

    /**
     * Set the value of Bairro
     *
     * @param mixed bairro
     *
     * @return self
     */
    public function setBairro(string $bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get the value of Cidade
     *
     * @return mixed
     */
    public function getCidade() : string
    {
        return $this->cidade;
    }

    /**
     * Set the value of Cidade
     *
     * @param mixed cidade
     *
     * @return self
     */
    public function setCidade(string $cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get the value of Cep
     *
     * @return mixed
     */
    public function getCep() : string
    {
        return $this->cep;
    }

    /**
     * Set the value of Cep
     *
     * @param mixed cep
     *
     * @return self
     */
    public function setCep(string $cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get the value of Coordenadas
     *
     * @return mixed
     */
    public function getCoordenada() : string
    {
        return $this->coordenadas;
    }

    /**
     * Set the value of Coordenadas
     *
     * @param mixed coordenadas
     *
     * @return self
     */
    public function setCoordenada(string $coordenadas)
    {
        $this->coordenadas = $coordenadas;

        return $this;
    }

}
