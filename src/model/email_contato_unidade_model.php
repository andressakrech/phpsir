<?php

  class EmailContatoUnidadeModel
  {

    private $idEmailContatoUnidade;
    private $email;
    private $idContatoUnidade;

    public function toArray()
    {

      return array(
        'id_email_contato_unidade' => $this->idEmailContatoUnidade,
        'email' => $this->email,
        '$id_contato_unidade' => $this->idEmailContatoUnidade
      );

    }

    /**
     * Get the value of Id Email Contato Unidade
     *
     * @return mixed
     */
    public function getIdEmailContatoUnidade() : int
    {
        return $this->idEmailContatoUnidade;
    }

    /**
     * Set the value of Id Email Contato Unidade
     *
     * @param mixed idEmailContatoUnidade
     *
     * @return self
     */
    public function setIdEmailContatoUnidade(int $idEmailContatoUnidade)
    {
        $this->idEmailContatoUnidade = $idEmailContatoUnidade;

        return $this;
    }

    /**
     * Get the value of Email
     *
     * @return mixed
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param mixed email
     *
     * @return self
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of Id Contato Unidade
     *
     * @return mixed
     */
    public function getIdContatoUnidade() : int
    {
        return $this->idContatoUnidade;
    }

    /**
     * Set the value of Id Contato Unidade
     *
     * @param mixed idContatoUnidade
     *
     * @return self
     */
    public function setIdContatoUnidade(int $idContatoUnidade)
    {
        $this->idContatoUnidade = $idContatoUnidade;

        return $this;
    }

}
