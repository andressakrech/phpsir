<?php

class Ipv6Model
{

  private $idIpv6;
  private $enderecoIpv6;
  private $idInterface;

  public function __construct($endereco, $idInterface) {
    $this->enderecoIpv6 = empty($endereco) ? "" : $endereco;
    $this->idInterface = $idInterface;
  }

  public function toArray()
  {
    return array('id_ipv6' => $this->idIpv6,
    'endereco_ipv6' => $this->enderecoIpv6,
    'id_interface' => $this->idInterface);
  }


    /**
     * Get the value of Id Ipv
     *
     * @return mixed
     */
    public function getIdIpv6() : int
    {
        return $this->idIpv6;
    }

    /**
     * Set the value of Id Ipv
     *
     * @param mixed idIpv6
     *
     * @return self
     */
    public function setIdIpv6(int $idIpv6)
    {
        $this->idIpv6 = $idIpv6;

        return $this;
    }

    /**
     * Get the value of Endereco Ipv
     *
     * @return mixed
     */
    public function getEnderecoIpv6() : string
    {
        return $this->enderecoIpv6;
    }

    /**
     * Set the value of Endereco Ipv
     *
     * @param mixed enderecoIpv6
     *
     * @return self
     */
    public function setEnderecoIpv6(string $enderecoIpv6)
    {
        if (filter_var($enderecoIpv6, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6))
        {
          $this->enderecoIpv6 = $enderecoIpv6;
        }
        return $this;
    }

    /**
     * Get the value of Id Interface
     *
     * @return mixed
     */
    public function getIdInterface() : int
    {
        return $this->idInterface;
    }

    /**
     * Set the value of Id Interface
     *
     * @param mixed idInterface
     *
     * @return self
     */
    public function setIdInterface(int $idInterface)
    {
        $this->idInterface = $idInterface;

        return $this;
    }

}
