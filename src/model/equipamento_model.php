<?php


class EquipamentoModel {

  private $idEquipamento;
  private $hostname;
  private $ipv4;
  private $ipv6;
  private $fabricante;
  private $modelo;
  private $comunidadeSnmp;
  private $mascara;
  private $idVersaoSnmp;
  private $interfaces;

  public function toArray() {
    return array(  'id_equipamento' => $this->idEquipamento,
        'hostname' => $this->hostname,
        'ipv4' => $this->ipv4,
        'ipv6' => $this->ipv6,
        'fabricante' => $this->fabricante,
        'modelo' => $this->modelo,
        'comunidade_snmp' => $this->comunidadeSnmp,
        'mascara' => $this->mascara,
        'id_versao_snmp' => $this->idVersaoSnmp,
        'interfaces' => $this->interfaces );
  }

    /**
     * Get the value of Id Equipamento
     *
     * @return mixed
     */
    public function getIdEquipamento() : int
    {
        return $this->idEquipamento;
    }

    /**
     * Set the value of Id Equipamento
     *
     * @param mixed idEquipamento
     *
     * @return self
     */
    public function setIdEquipamento(int $idEquipamento)
    {
        $this->idEquipamento = $idEquipamento;

        return $this;
    }

    /**
     * Get the value of Hostname
     *
     * @return mixed
     */
    public function getHostname() : string
    {
        return $this->hostname;
    }

    /**
     * Set the value of Hostname
     *
     * @param mixed hostname
     *
     * @return self
     */
    public function setHostname(string $hostname)
    {
        $this->hostname = $hostname;

        return $this;
    }

    /**
     * Get the value of Ipv
     *
     * @return mixed
     */
    public function getIpv4() : string
    {
        return $this->ipv4;
    }

    /**
     * Set the value of Ipv
     *
     * @param mixed ipv4
     *
     * @return self
     */
    public function setIpv4(string $ipv4)
    {
        include_once(ABSPATH.'/functions/valida_ipv4.php');
        if (validaIpv4($ipv4)) {
           $this->ipv4 = $ipv4;
        }

        return $this;
    }

    /**
     * Get the value of Ipv
     *
     * @return mixed
     */
    public function getIpv6() : string
    {
        return $this->ipv6;
    }

    /**
     * Set the value of Ipv
     *
     * @param mixed ipv6
     *
     * @return self
     */
    public function setIpv6(string $ipv6)
    {
        include_once(ABSPATH.'/functions/valida_ipv6.php');
        if (validaIpv6($ipv6)) {
          $this->ipv6 = $ipv6;
        } else {
          $this->ipv6 = "";
        }

        return $this;
    }

    /**
     * Get the value of Fabricante
     *
     * @return mixed
     */
    public function getFabricante() : string
    {
        return $this->fabricante;
    }

    /**
     * Set the value of Fabricante
     *
     * @param mixed fabricante
     *
     * @return self
     */
    public function setFabricante(string $fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get the value of Modelo
     *
     * @return mixed
     */
    public function getModelo() : string
    {
        return $this->modelo;
    }

    /**
     * Set the value of Modelo
     *
     * @param mixed modelo
     *
     * @return self
     */
    public function setModelo(string $modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get the value of Comunidade Snmp
     *
     * @return mixed
     */
    public function getComunidadeSnmp() : string
    {
        return $this->comunidadeSnmp;
    }

    /**
     * Set the value of Comunidade Snmp
     *
     * @param mixed comunidadeSnmp
     *
     * @return self
     */
    public function setComunidadeSnmp(string $comunidadeSnmp)
    {
        $this->comunidadeSnmp = $comunidadeSnmp;

        return $this;
    }

    public function getMascara() : string
    {
        return $this->mascara;
    }

    public function setMascara(string $mascara)
    {
        $this->mascara = $mascara;

        return $this;
    }

    /**
     * Get the value of Id Versao
     *
     * @return mixed
     */
    public function getIdVersaoSnmp() : int
    {
        return $this->idVersaoSnmp;
    }

    /**
     * Set the value of Id Versao
     *
     * @param mixed idVersao
     *
     * @return self
     */
    public function setIdVersaoSnmp(int $idVersaoSnmp)
    {
        $this->idVersaoSnmp = $idVersaoSnmp;

        return $this;
    }

    public function getInterfaces() : array
    {
        return $this->interfaces;
    }

    public function setInterfaces(array $interfaces)
    {
        $this->interfaces = $interfaces;

        return $this;
    }

}
