<?php

class EmailContatoOperadoraModel
{
  private $idEmailContatoOperadora;
  private $email;
  private $idContatoOperadora;

  public function toArray()
  {
    return array(
      'id_email_contato_operadora' => $this->idEmailContatoOperadora,
      'email' => $this->email,
      'id_contato_operadora' => $this->idContatoOperadora
    );
  }

    /**
     * Get the value of Id Email Contato Operadora
     *
     * @return mixed
     */
    public function getIdEmailContatoOperadora() : int
    {
        return $this->idEmailContatoOperadora;
    }

    /**
     * Set the value of Id Email Contato Operadora
     *
     * @param mixed idEmailContatoOperadora
     *
     * @return self
     */
    public function setIdEmailContatoOperadora(int $idEmailContatoOperadora)
    {
        $this->idEmailContatoOperadora = $idEmailContatoOperadora;

        return $this;
    }

    /**
     * Get the value of Email
     *
     * @return mixed
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param mixed email
     *
     * @return self
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of Id Contato Operadora
     *
     * @return mixed
     */
    public function getIdContatoOperadora() : int
    {
        return $this->idContatoOperadora;
    }

    /**
     * Set the value of Id Contato Operadora
     *
     * @param mixed idContatoOperadora
     *
     * @return self
     */
    public function setIdContatoOperadora(int $idContatoOperadora)
    {
        $this->idContatoOperadora = $idContatoOperadora;

        return $this;
    }

}
