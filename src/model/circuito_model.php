<?php

class CircuitoModel {

  private $idCircuito;
  private $idOperadora;
  private $idUnidade;
  private $designacao;
  private $banda;
  private $dataAtivacao;
  private $valorMensal;//double
  private $ativo;
  private $idConexao;

  public function toArray()
  {
    return array('id_circuito' => $this->idCircuito,
    'id_operadora' => $this->idOperadora,
    'id_unidade' => $this->idUnidade,
    'designacao' => $this->designacao,
    'banda' => $this->banda,
    'data_ativacao' => $this->dataAtivacao,
    'valor_mensal' => $this->valorMensal,
    'ativo' => $this->ativo,
    'id_conexao' => $this->idConexao);

  }

    /**
     * Get the value of Id Circuito
     *
     * @return mixed
     */
    public function getIdCircuito() : int
    {
        return $this->idCircuito;
    }

    /**
     * Set the value of Id Circuito
     *
     * @param mixed idCircuito
     *
     * @return self
     */
    public function setIdCircuito(int $idCircuito)
    {
        $this->idCircuito = $idCircuito;

        return $this;
    }

    /**
     * Get the value of Id Operadora
     *
     * @return mixed
     */
    public function getIdOperadora() : int
    {
        return $this->idOperadora;
    }

    /**
     * Set the value of Id Operadora
     *
     * @param mixed idOperadora
     *
     * @return self
     */
    public function setIdOperadora(int $idOperadora)
    {
        $this->idOperadora = $idOperadora;

        return $this;
    }

    /**
     * Get the value of Id Unidade
     *
     * @return mixed
     */
    public function getIdUnidade() : int
    {
        return $this->idUnidade;
    }

    /**
     * Set the value of Id Unidade
     *
     * @param mixed idUnidade
     *
     * @return self
     */
    public function setIdUnidade(int $idUnidade)
    {
        $this->idUnidade = $idUnidade;

        return $this;
    }

    /**
     * Get the value of Designacao
     *
     * @return mixed
     */
    public function getDesignacao() : string
    {
        return $this->designacao;
    }

    /**
     * Set the value of Designacao
     *
     * @param mixed designacao
     *
     * @return self
     */
    public function setDesignacao(string $designacao)
    {
        $this->designacao = $designacao;

        return $this;
    }

    /**
     * Get the value of Banda
     *
     * @return mixed
     */
    public function getBanda() : int
    {
        return $this->banda;
    }

    /**
     * Set the value of Banda
     *
     * @param mixed banda
     *
     * @return self
     */
    public function setBanda(int $banda)
    {
        $this->banda = $banda;

        return $this;
    }

    /**
     * Get the value of Data Ativacao
     *
     * @return mixed
     */
    public function getDataAtivacao() : string
    {
        return $this->dataAtivacao;
    }

    /**
     * Set the value of Data Ativacao
     *
     * @param mixed dataAtivacao
     *
     * @return self
     */
    public function setDataAtivacao(string $dataAtivacao)
    {
        $this->dataAtivacao = $dataAtivacao;

        return $this;
    }

    /**
     * Get the value of Valor Mensal
     *
     * @return mixed
     */
    public function getValorMensal() : float
    {
        return $this->valorMensal;
    }

    /**
     * Set the value of Valor Mensal
     *
     * @param mixed valorMensal
     *
     * @return self
     */
    public function setValorMensal(float $valorMensal)
    {
        $this->valorMensal = $valorMensal;

        return $this;
    }

    /**
     * Get the value of Ativo
     *
     * @return mixed
     */
    public function getAtivo() : int
    {
        return $this->ativo;
    }

    /**
     * Set the value of Ativo
     *
     * @param mixed ativo
     *
     * @return self
     */
    public function setAtivo(int $ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * Get the value of Conexao Id Conexao
     *
     * @return mixed
     */
    public function getIdConexao() : int
    {
        return $this->idConexao;
    }

    /**
     * Set the value of Conexao Id Conexao
     *
     * @param mixed idConexao
     *
     * @return self
     */
    public function setIdConexao(int $idConexao)
    {
        $this->idConexao = $idConexao;

        return $this;
    }

}
