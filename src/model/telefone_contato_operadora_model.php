<?php

class TelefoneContatoOperadoraModel
{
  private $idTelefoneContatoOperadora;
  private $ddd;
  private $numero;
  private $idContatoOperadora;

  public function toArray()
  {
    return array(
      'id_telefone_contato_operadora' => $this->idTelefoneContatoOperadora,
      'ddd' => $this->ddd,
      'numero' => $this->numero,
      'id_contato_operadora' => $this->idContatoOperadora
    );
  }

    /**
     * Get the value of Id Telefone Operadora
     *
     * @return mixed
     */
    public function getIdTelefoneOperadora()
    {
        return $this->idTelefoneContatoOperadora;
    }

    /**
     * Set the value of Id Telefone Operadora
     *
     * @param mixed idTelefoneOperadora
     *
     * @return self
     */
    public function setIdTelefoneContatoOperadora($idTelefoneOperadora)
    {
        $this->idTelefoneContatoOperadora = $idTelefoneOperadora;

        return $this;
    }

    /**
     * Get the value of Ddd
     *
     * @return mixed
     */
    public function getDdd()
    {
        return $this->ddd;
    }

    /**
     * Set the value of Ddd
     *
     * @param mixed ddd
     *
     * @return self
     */
    public function setDdd($ddd)
    {
        $this->ddd = $ddd;

        return $this;
    }

    /**
     * Get the value of Numero
     *
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set the value of Numero
     *
     * @param mixed numero
     *
     * @return self
     */
    public function setNumero($numero)
    {
        preg_match('/^(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/', $numero) ? $this->numero = $numero : "";
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get the value of Id Contato Operadora
     *
     * @return mixed
     */
    public function getIdContatoOperadora()
    {
        return $this->idContatoOperadora;
    }

    /**
     * Set the value of Id Contato Operadora
     *
     * @param mixed idContatoOperadora
     *
     * @return self
     */
    public function setIdContatoOperadora($idContatoOperadora)
    {
        $this->idContatoOperadora = $idContatoOperadora;

        return $this;
    }

}
