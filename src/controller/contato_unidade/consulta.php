<?php
//carrega contatoUnidades
include_once(ABSPATH.'/dao/contato_unidade_dao.php');



if (isset($_GET['id'])) {
  $contatoUnidadeDao = new ContatoUnidadeDao();
  $contatoUnidadeModel = $contatoUnidadeDao->getById($_GET['id'])[0];
  $contatoUnidade = $contatoUnidadeModel->toArray();

  include_once(ABSPATH.'/view/contato_unidade_view.php');
  $view = new ContatoUnidadeView();
  $view->consulta($contatoUnidade);
} else {
  //new Message(2, "É necessário informar o id para está requisição");
  include_once ABSPATH."/view/404.php";
}
