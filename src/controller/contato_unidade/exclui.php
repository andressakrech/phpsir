<?php
//carrega contato_unidades
include_once(ABSPATH.'/dao/contato_unidade_dao.php');

$contatoUnidadeDao = new ContatoUnidadeDao();

$contatoModel = $contatoUnidadeDao->getById($_GET['id'])[0];

$idUnidade = $contatoModel->getIdUnidade();

//se os dados obrigatorios foram passados
if (isset($_GET['id'])) {
  //remove o contato_unidade do banco
  ($contatoUnidadeDao->excluir($_GET['id']) !== null) ? new Message(array(0,'Contato removido com sucesso.')) : new Message(array(1,'Não foi possível remover o contato.'));
} else {
  new Message( array(3,'É necessário informar o identificador do contato para excluir.') );
}

include_once(ABSPATH.'/dao/unidade_dao.php');
$unidadeDao = new UnidadeDao();
$unidadeModel = $unidadeDao->getById($idUnidade)[0];
$unidade = $unidadeModel->toArray();

include_once(ABSPATH.'/dao/instituicao_dao.php');
$instituicaoDao = new InstituicaoDao();
$instituicoes = array();

foreach ($instituicaoDao->listar() as $i) {
  $instituicoes[$i->getIdInstituicao()] = $i->getInstituicaoNome();
}

include_once(ABSPATH.'/dao/contato_unidade_dao.php');
$contatoUnidadeDao = new ContatoUnidadeDao();
$contatos = array();

foreach ($contatoUnidadeDao->getByIdUnidade($idUnidade) as $c) {
  $contatos[$c->getIdContatoUnidade()] = $c->toArray();
}

include_once(ABSPATH.'/view/unidade_view.php');
$view = new UnidadeView();
$view->consulta($unidade, $instituicoes, $contatos);

