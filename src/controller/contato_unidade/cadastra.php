<?php
//carrega enquadramentos
//include_once(ABSPATH.'/dao/unidade_dao.php');
include_once(ABSPATH.'/dao/contato_unidade_dao.php');

//$unidadeDao = new UnidadeDao();
$contatoDao = new ContatoUnidadeDao();

$contatoModel = new ContatoUnidadeModel();

include_once(ABSPATH.'/view/contato_unidade_view.php');
$view = new ContatoUnidadeView();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados. obs: Deve ser passado ao menos 1 email ou telefone
  if (isset($_POST['nome']) && isset($_GET['unidade']) && ($_POST['telefone'] || $_POST['email'])) {

    $contatoModel->setNome($_POST['nome']);
    $contatoModel->setObservacao($_POST['observacao']);
    $contatoModel->setTecnico(isset($_POST['tecnico'])?1:0);
    $contatoModel->setSeguranca(isset($_POST['seguranca'])?1:0);
    $contatoModel->setAdministrativo(isset($_POST['administrativo'])?1:0);
    $contatoModel->setEmail($_POST['email']);
    $contatoModel->setTelefone($_POST['telefone']);
    $contatoModel->setIdUnidade($_GET['unidade']);
    
    //insere contato no banco
    ($contatoDao->inserir($contatoModel)) ? new Message(array(0,'Contato cadastrado com sucesso.')) : new Message(array(1,'Ocorreu um erro ao cadastrar o contato.'));

  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

// inclui a view para exibir os dados
$view->cadastra();

