<?php
//carrega enquadramentos
include_once(ABSPATH.'/dao/enquadramento_dao.php');

$enquadramentoDao = new EnquadramentoDao();

$enquadramentos = array();

foreach($enquadramentoDao->listar() as $e) {
  $enquadramentos[] = $e->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/enquadramento_view.php');
$view = new EnquadramentoView();
$view->lista($enquadramentos);

