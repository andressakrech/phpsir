<?php
//carrega enquadramentos
include_once(ABSPATH.'/dao/enquadramento_dao.php');

if (isset($_GET['id'])) {
  $enquadramentoDao = new EnquadramentoDao();
  $enquadramentoModel = $enquadramentoDao->getById($_GET['id'])[0];
  $enquadramento = $enquadramentoModel->toArray();

  include_once(ABSPATH.'/view/enquadramento_view.php');
  $view = new EnquadramentoView();
  $view->consulta($enquadramento);
} else {
  //new Message(2, "É necessário informar o id para está requisição");
  include_once ABSPATH."/view/404.php";
}
