<?php
//carrega enquadramentos
include_once(ABSPATH.'/dao/enquadramento_dao.php');

$enquadramentoDao = new EnquadramentoDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if (isset($_POST['enquadramento_nome'])) {
    $enquadramentoModel = new EnquadramentoModel();
    //adiciona os campos
    $enquadramentoModel->setEnquadramentoNome($_POST['enquadramento_nome']);

    //insere o enquadramento no banco
    ($enquadramentoDao->inserir($enquadramentoModel) !== null) ? new Message(array(0,'Enquadramento cadastrado com sucesso.')) : new Message(array(1,'Ocorreu um erro ao cadastrar o enquadramento.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/enquadramento_view.php');
$view = new EnquadramentoView();
$view->cadastra();

