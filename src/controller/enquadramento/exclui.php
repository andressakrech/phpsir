<?php
//carrega enquadramentos
include_once(ABSPATH.'/dao/enquadramento_dao.php');

$enquadramentoDao = new EnquadramentoDao();

//se os dados obrigatorios foram passados
if (isset($_GET['id'])) {
  //remove o enquadramento do banco
  ($enquadramentoDao->excluir($_GET['id']) !== null) ? new Message(array(0,'Enquadramento removido com sucesso.')) : new Message(array(1,'Não foi possível remover o enquadramento.'));
} else {
  new Message( array(3,'É necessário informar o identificador do enquadramento para excluir.') );
}

$enquadramentos = array();

foreach($enquadramentoDao->listar() as $e) {
  $enquadramentos[] = $e->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/enquadramento_view.php');
$view = new EnquadramentoView();
$view->lista($enquadramentos);

