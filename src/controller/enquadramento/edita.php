<?php
//carrega enquadramentos
include_once(ABSPATH.'/dao/enquadramento_dao.php');

$enquadramentoDao = new EnquadramentoDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if (isset($_POST['enquadramento_nome']) && isset($_GET['id'])) {
    $enquadramentoModel = new EnquadramentoModel();
    //adiciona os campos
    $enquadramentoModel->setIdEnquadramento($_GET['id']);
    $enquadramentoModel->setEnquadramentoNome($_POST['enquadramento_nome']);

    //insere o enquadramento no banco
    ($enquadramentoDao->atualizar($enquadramentoModel) !== null) ? new Message(array(0,'Enquadramento atualizado com sucesso.')) : new Message(array(1,'Ocorreu um erro ao atualizar o enquadramento.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}
//se a página for edição carrega as informações do db, senão passa um array vazio para preenchimento
$enquadramento = $enquadramentoDao->getById($_GET['id'])[0]->toArray();

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/enquadramento_view.php');
$view = new EnquadramentoView();
$view->edita($enquadramento);

