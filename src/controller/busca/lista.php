<?php
//carrega enquadramentos

$busca = $_POST['busca'];

// inicio busca instituicoes

include_once(ABSPATH.'/dao/instituicao_dao.php');

$instituicaoDao = new InstituicaoDao();

$inst = $instituicaoDao->getLikeSigla($busca);
//print_r($inst);

if (!empty($inst)):

  $instituicoes = array();
  foreach ($inst as $i) {
    $instituicoes[] = $i->toArray();
  }

  include_once(ABSPATH.'/dao/enquadramento_dao.php');
  $enquadramentoDao = new EnquadramentoDao();
  $enquadramentos = array();

  foreach ($enquadramentoDao->listar() as $e) {
    $enquadramentos[$e->getIdEnquadramento()] = $e->getEnquadramentoNome();
  }

  include_once(ABSPATH.'/view/instituicao_view.php');
  $view = new InstituicaoView();
  $view->lista($instituicoes, $enquadramentos);

endif; // fim busca instituicao

//inicio busca unidade

include_once(ABSPATH.'/dao/unidade_dao.php');
include_once(ABSPATH.'/dao/instituicao_dao.php');

$unidadeDao = new UnidadeDao();
$instituicaoDao = new InstituicaoDao();

$unid = $unidadeDao->getLikeSigla($busca);
if (!empty($unid)):

  // lista as unidades para carregar na view
  $unidades = array();
  foreach ($unid as $u) {
    $unidades[] = $u->toArray();
  }

  // lista as instituicoes para carregar na view
  $instituicoes = array();
  foreach ($instituicaoDao->listar() as $i) {
    $instituicoes[$i->getIdInstituicao()] = $i->toArray();
  }

  // inclui a view para exibir os dados
  include_once(ABSPATH.'/view/unidade_view.php');
  $view = new UnidadeView();
  $view->lista($unidades, $instituicoes);

endif; // fim busca unidades



//busca equipamentos

include_once(ABSPATH.'/dao/equipamento_dao.php');

$equipamentoDao = new EquipamentoDao();

// lista as equipamentos para carregar na view
$equipamentoModel = $equipamentoDao->getLikeHostname($busca);

// se o equipametno existir, listaa
if (!empty($equipamentoModel)) {

$equipamentos = array();
foreach ($equipamentoModel as $e) {
  $equipamentos[] = $e->toArray();
}


// inclui a view para exibir os dados
include_once(ABSPATH.'/view/equipamento_view.php');
$view = new EquipamentoView();
$view->lista($equipamentos);

} //fim lista equipamentos
