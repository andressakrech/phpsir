<?php
//carrega equipamentos
include_once(ABSPATH.'/dao/equipamento_dao.php');

$equipamentoDao = new EquipamentoDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if (isset($_POST['hostname']) && isset($_GET['id']) && isset($_POST['ipv4']) && isset($_POST['comunidade_snmp']) && isset($_POST['id_versao_snmp'])) {
    $equipamentoModel = new EquipamentoModel();
    //adiciona os campos
    $equipamentoModel->setIdEquipamento($_GET['id']);
    $equipamentoModel->setHostName($_POST['hostname']);
    $equipamentoModel->setIpv4($_POST['ipv4']);
    $equipamentoModel->setIpv6($_POST['ipv6']);
    $equipamentoModel->setFabricante($_POST['fabricante']);
    $equipamentoModel->setModelo($_POST['modelo']);
    $equipamentoModel->setComunidadeSnmp($_POST['comunidade_snmp']);
    $equipamentoModel->setMascara($_POST['mascara']);
    $equipamentoModel->setIdVersaoSnmp($_POST['id_versao_snmp']);
    
    if (validaIpv4($_POST['ipv4'])) {
    //insere o equipamento no banco
      ($equipamentoDao->atualizar($equipamentoModel) !== null) ? new Message(array(0,'Equipamento atualizado com sucesso.')) : new Message(array(1,'Ocorreu um erro ao atualizar o equipamento.'));
    } else {
      new Message(array(3, 'Endereço Ipv4 é invalido. Por favor informe um endereço valido.'));
    }
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}
// carrega os dados para serem exibidos
$equipamento = $equipamentoDao->getById($_GET['id'])[0]->toArray();


include_once(ABSPATH.'/dao/versao_snmp_dao.php');
$versaoDao = new VersaoSnmpDao();
$versoes = array();

foreach ($versaoDao->listar() as $v) {
  $versoes[$v->getIdVersaoSnmp()] = $v->getVersaoSnmp();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/equipamento_view.php');
$view = new EquipamentoView();
$view->edita($equipamento, $versoes);
