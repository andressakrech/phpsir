<?php
//carrega equipamentos
include_once(ABSPATH.'/dao/equipamento_dao.php');
include_once(ABSPATH.'/functions/consulta_snmp.php');

$equipamentoDao = new EquipamentoDao();

// verifica os equipametnos que vai buscar, se for passado o id é apenas o informado, senao todos
if (!isset($_GET['id'])) {
  $idEquipamento = $_GET['id'];
  new Message(array(2, "É necessário informar o id para está requisição"));
  include_once ABSPATH."/view/404.php";
  exit(0);
}

$idEquipamento = $_GET['id'];

$equipamentoModel = $equipamentoDao->getById($idEquipamento)[0];

include_once(ABSPATH.'/dao/versao_snmp_dao.php');
$versaoSnmpDao = new VersaoSnmpDao();
$versaoSnmp = $versaoSnmpDao->getById($equipamentoModel->getIdVersaoSnmp())[0]->getVersaoSnmp();
/*
$equipamentoModel->setInterfaces(buscaSnmp($versaoSnmp, $equipamentoModel->getComunidadeSnmp(), $equipamentoModel->getIpv4()));

  
}
*/
include_once(ABSPATH.'/dao/interface_dao.php');
include_once(ABSPATH.'/dao/ipv4_dao.php');
include_once(ABSPATH.'/dao/ipv6_dao.php');
$interfaceDao = new InterfaceDao();
$ipv4Dao = new Ipv4Dao();
$ipv6Dao = new Ipv6Dao();
$interfaces = array();
$equipamentoModel->setInterfaces(buscaSnmp($versaoSnmp, $equipamentoModel->getComunidadeSnmp(), $equipamentoModel->getIpv4()));
foreach ($equipamentoModel->getInterfaces() as $ifIndex=>$i) {
  // $i = Array ( [ifName] => "Bundle-Ether2.44" [ifDescription] => "Bundle-Ether2.44" [ifAlias] => "RedeTche-v6-EXEMPLO-BUNDLE::::" [ipv4] => Array ( ) [ipv6] => Array ( [0] => 2804:0:0:bac::c120 [1] => fe80::226:caff:feab:4384 ) ) 
  //$interfaceModel = new InterfaceModel($ifIndex, $i['ifAlias'], $i['ifName'], $i['ifDescription'], $idEquipamento, $i['ipv4'], $i['ipv6']);
  $interfaceModel = new InterfaceModel($i['ifIndex'], $i['ifAlias'], $i['ifName'], $i['ifDescription'], $idEquipamento, $i['ipv4'], $i['ipv6']);
  $idInterface = $interfaceDao->inserir($interfaceModel);
  if ($idInterface == null) {
    new Message(array(1, "ocorreu um erro ao inserir a interface."));
    include_once ABSPATH."/view/404.php";
    exit(0);
  }
  foreach ($i['ipv4'] as $e) {
    $ipv4Model = new Ipv4Model($e, $idInterface);
  }
  foreach ($i['ipv6'] as $e) {
    $ipv6Model = new Ipv6Model($e, $idInterface);
  }

  
  $interfaces[] = $interfaceModel;
  
}

new Message(array(0, "Interfaces da equipamento {$equipamentoModel->getHostname()} atualizadas com sucesso."));

include_once(ABSPATH.'/view/interface_view.php');
$view = new InterfaceView();
$view->lista($equipamentoModel->toArray());

/*

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/equipamento_view.php');
$view = new EquipamentoView();
$view->cadastra($versaoSnmp);
*/
