<?php
//carrega equipamentos
include_once(ABSPATH.'/dao/equipamento_dao.php');

if (!isset($_GET['id'])) {
  //new Message(2, "É necessário informar o id para está requisição");
  include_once ABSPATH."/view/404.php";
  exit(0);
}

$equipamentoDao = new EquipamentoDao();
$equipamentoModel = $equipamentoDao->getById($_GET['id'])[0];
$equipamento = $equipamentoModel->toArray();

include_once(ABSPATH.'/dao/versao_snmp_dao.php');
$versaoSnmpDao = new VersaoSnmpDao();
$versoesSnmp = array();

foreach ($versaoSnmpDao->listar() as $v) {
  $versoesSnmp[$v->getIdVersaoSnmp()] = $v->getVersaoSnmp();
}

include_once(ABSPATH.'/view/equipamento_view.php');
$view = new EquipamentoView();
$view->consulta($equipamento, $versoesSnmp);
