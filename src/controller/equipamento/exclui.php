<?php
//carrega equipamentos
include_once(ABSPATH.'/dao/equipamento_dao.php');

$equipamentoDao = new EquipamentoDao();

//se os dados obrigatorios foram passados
if (isset($_GET['id'])) {
  //remove o equipamento do banco
  ($equipamentoDao->excluir($_GET['id']) !== null) ? new Message(array(0,'Equipamento removido com sucesso.')) : new Message(array(1,'Não foi possível remover o equipamento.'));
} else {
  new Message( array(3,'É necessário passar o identificador da instituição para excluir.') );
}

$equipamentos = array();

foreach($equipamentoDao->listar() as $e) {
  $equipamentos[] = $e->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/equipamento_view.php');
$view = new EquipamentoView();
$view->lista($equipamentos);

