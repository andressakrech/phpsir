<?php

include_once(ABSPATH.'/dao/equipamento_dao.php');

$equipamentoDao = new EquipamentoDao();

// lista as equipamentos para carregar na view
$equipamentos = array();

foreach ($equipamentoDao->listar() as $e) {
  $equipamentos[] = $e->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/equipamento_view.php');
$view = new EquipamentoView();
$view->lista($equipamentos);
