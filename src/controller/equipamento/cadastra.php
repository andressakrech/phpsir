<?php
//carrega equipamentos
include_once(ABSPATH.'/dao/equipamento_dao.php');
include_once(ABSPATH.'/functions/valida_ipv4.php');

$equipamentoDao = new EquipamentoDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados

  if (isset($_POST['hostname']) && isset($_POST['ipv4']) && isset($_POST['comunidade_snmp']) && isset($_POST['id_versao_snmp'])) {
    $equipamentoModel = new EquipamentoModel();
    //adiciona os campos
    $equipamentoModel->setHostname($_POST['hostname']);
    $equipamentoModel->setIpv4($_POST['ipv4']);
    $equipamentoModel->setIpv6($_POST['ipv6']);
    $equipamentoModel->setFabricante($_POST['fabricante']);
    $equipamentoModel->setModelo($_POST['modelo']);
    $equipamentoModel->setComunidadeSnmp($_POST['comunidade_snmp']);
    $equipamentoModel->setMascara($_POST['mascara']);
    $equipamentoModel->setIdVersaoSnmp($_POST['id_versao_snmp']);

    //insere o equipamento no banco
    if (validaIpv4($_POST['ipv4'])) {
    ($equipamentoDao->inserir($equipamentoModel) !== null) ? new Message(array(0,'Equipamento cadastrada com sucesso.')) : new Message(array(1,'Ocorreu um erro ao cadastrar o Equipamento.'));
    } else {
      new Message( array(3, 'O endereço Ipv4 inserido não é valido. Por favor preencher um valor valido.') );
    }
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

include_once(ABSPATH.'/dao/versao_snmp_dao.php');
$versaoSnmpDao = new VersaoSnmpDao();
$versaoSnmp = array();

foreach ($versaoSnmpDao->listar() as $v) {
  $versaoSnmp[] = $v->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/equipamento_view.php');
$view = new EquipamentoView();
$view->cadastra($versaoSnmp);

