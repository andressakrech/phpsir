<?php
//carrega operadoras
include_once(ABSPATH.'/dao/operadora_dao.php');

if (!isset($_GET['id'])) {
  //new Message(2, "É necessário informar o id para está requisição");
  include_once ABSPATH."/view/404.php";
  exit(0);
}

$operadoraDao = new OperadoraDao();
$operadoraModel = $operadoraDao->getById($_GET['id'])[0];
$operadora = $operadoraModel->toArray();



include_once(ABSPATH.'/dao/contato_operadora_dao.php');
$contatoOperadoraDao = new ContatoOperadoraDao();
$contatos = array();

foreach ($contatoOperadoraDao->getByIdOperadora($_GET['id']) as $c) {
  $contatos[$c->getIdContatoOperadora()] = $c->toArray();
}

//print_r($contatos);

include_once(ABSPATH.'/view/operadora_view.php');
$view = new OperadoraView();
$view->consulta($operadora);
unset($view);

include_once(ABSPATH.'/view/contato_operadora_view.php');
$view = new ContatoOperadoraView();
$view->lista($contatos, $_GET['id']);
