<?php
//carrega operadoras
include_once(ABSPATH.'/dao/operadora_dao.php');

$operadoraDao = new OperadoraDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if ( isset($_POST['operadora_nome']) && isset($_POST['operadora_sigla']) ) {
    $operadoraModel = new OperadoraModel();
    //adiciona os campos
    $operadoraModel->setOperadoraNome($_POST['operadora_nome']);
    $operadoraModel->setOperadoraSigla($_POST['operadora_sigla']);
    $operadoraModel->setSite($_POST['site']);
    $operadoraModel->setObservacao($_POST['observacao']);

    //insere o operadora no banco
    ($operadoraDao->inserir($operadoraModel) !== null) ? new Message(array(0,'Operadora cadastrada com sucesso.')) : new Message(array(1,'Ocorreu um erro ao cadastrar a operadora.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}


// inclui a view para exibir os dados
include_once(ABSPATH.'/view/operadora_view.php');
$view = new OperadoraView();
$view->cadastra();
