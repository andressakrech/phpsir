<?php
include_once(ABSPATH.'/dao/operadora_dao.php');

$operadoraDao = new OperadoraDao();

// lista as operadoras para carregar na view
$operadoras = array();
foreach ($operadoraDao->listar() as $u) {
  $operadoras[] = $u->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/operadora_view.php');
$view = new OperadoraView();
$view->lista($operadoras);
