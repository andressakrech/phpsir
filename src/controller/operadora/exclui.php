<?php
//carrega operadoras
include_once(ABSPATH.'/dao/operadora_dao.php');

$operadoraDao = new operadoraDao();

//se os dados obrigatorios foram passados
if (isset($_GET['id'])) {
  //remove o operadora do banco
  ($operadoraDao->excluir($_GET['id'])) ? new Message(array(0,'operadora removida com sucesso.')) : new Message(array(1,'Não foi possível remover a operadora.'));
} else {
  new Message( array(3,'É necessário passar o identificador da operadora para excluir.') );
}

$operadoras = array();

foreach($operadoraDao->listar() as $u) {
  $operadoras[] = $u->toArray();
}

$instituicoes = array();

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/operadora_view.php');
$view = new operadoraView();
$view->lista($operadoras);
