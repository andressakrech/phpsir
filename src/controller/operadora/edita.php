<?php
//carrega operadoras
include_once(ABSPATH.'/dao/operadora_dao.php');

$operadoraDao = new OperadoraDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if ( isset($_POST['operadora_nome']) && isset($_POST['operadora_sigla']) && isset($_GET['id'])) {
    $operadoraModel = new OperadoraModel();
    //adiciona os campos
    $operadoraModel->setIdOperadora($_GET['id']);
    $operadoraModel->setOperadoraNome($_POST['operadora_nome']);
    $operadoraModel->setOperadoraSigla($_POST['operadora_sigla']);
    $operadoraModel->setSite($_POST['site']);
    $operadoraModel->setObservacao($_POST['observacao']);

    //insere o operadora no banco
    ($operadoraDao->atualizar($operadoraModel)) ? new Message(array(0,'Operadora atualizada com sucesso.')) : new Message(array(1,'Ocorreu um erro ao atualizar a operadora.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

// verifica se o id da operadora existe senao encaminha para pagina de not found
if ($operadoraDao->getById($_GET['id']) == null) {
  //new Message(1, "O id especificado nao existe.");
  include_once(ABSPATH.'/view/404.php');
  exit(0);
}

$operadora = ($operadoraDao->getById($_GET['id'])[0])->toArray();

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/operadora_view.php');
$view = new OperadoraView();
$view->edita($operadora);
