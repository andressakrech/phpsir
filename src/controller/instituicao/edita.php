<?php
//carrega instituicoes
include_once(ABSPATH.'/dao/instituicao_dao.php');

$instituicaoDao = new InstituicaoDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if (isset($_POST['instituicao_nome']) && isset($_GET['id']) && isset($_POST['instituicao_sigla']) && isset($_POST['id_enquadramento'])) {
    $instituicaoModel = new InstituicaoModel();
    //adiciona os campos
    $instituicaoModel->setIdInstituicao($_GET['id']);
    $instituicaoModel->setInstituicaoNome($_POST['instituicao_nome']);
    $instituicaoModel->setInstituicaoSigla($_POST['instituicao_sigla']);
    $instituicaoModel->setInstituicaoSite($_POST['instituicao_site']);
    $instituicaoModel->setIdEnquadramento($_POST['id_enquadramento']);
    //$instituicaoModel->setReitoria($_POST['reitoria']);

    //insere o instituicao no banco
    ($instituicaoDao->atualizar($instituicaoModel) !== null) ? new Message(array(0,'Instituicao atualizado com sucesso.')) : new Message(array(1,'Ocorreu um erro ao atualizar o instituicao.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}
// carrega os dados atuais da instituição para serem exibidos
$instituicao = $instituicaoDao->getById($_GET['id'])[0]->toArray();


include_once(ABSPATH.'/dao/enquadramento_dao.php');
$enquadramentoDao = new EnquadramentoDao();
$enquadramentos = array();

foreach ($enquadramentoDao->listar() as $e) {
  $enquadramentos[$e->getIdEnquadramento()] = $e->getEnquadramentoNome();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/instituicao_view.php');
$view = new InstituicaoView();
$view->edita($instituicao, $enquadramentos);
