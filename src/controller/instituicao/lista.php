<?php

include_once(ABSPATH.'/dao/instituicao_dao.php');

$instituicaoDao = new InstituicaoDao();

// lista as instituicoes para carregar na view
$instituicoes = array();

foreach ($instituicaoDao->listar() as $i) {
  $instituicoes[] = $i->toArray();
}

include_once(ABSPATH.'/dao/enquadramento_dao.php');
$enquadramentoDao = new EnquadramentoDao();
$enquadramentos = array();

foreach ($enquadramentoDao->listar() as $e) {
  $enquadramentos[$e->getIdEnquadramento()] = $e->getEnquadramentoNome();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/instituicao_view.php');
$view = new InstituicaoView();
$view->lista($instituicoes, $enquadramentos);
