<?php
//carrega instituicoes
include_once(ABSPATH.'/dao/instituicao_dao.php');

$instituicaoDao = new InstituicaoDao();

//se os dados obrigatorios foram passados
if (isset($_GET['id'])) {
  //remove o instituicao do banco
  ($instituicaoDao->excluir($_GET['id']) !== null) ? new Message(array(0,'Instituicao removido com sucesso.')) : new Message(array(1,'Não foi possível remover instituição.'));
} else {
  new Message( array(3,'É necessário passar o identificador da instituição para excluir.') );
}

$instituicoes = array();

foreach($instituicaoDao->listar() as $e) {
  $instituicoes[] = $e->toArray();
}

include_once(ABSPATH.'/dao/enquadramento_dao.php');
$enquadramentoDao = new EnquadramentoDao();
$enquadramentos = array();

foreach ($enquadramentoDao->listar() as $e) {
  $enquadramentos[$e->getIdEnquadramento()] = $e->getEnquadramentoNome();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/instituicao_view.php');
$view = new InstituicaoView();
$view->lista($instituicoes, $enquadramentos);

