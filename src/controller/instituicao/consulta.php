<?php
//carrega instituicoes
include_once(ABSPATH.'/dao/instituicao_dao.php');

if (!isset($_GET['id'])) {
  //new Message(2, "É necessário informar o id para está requisição");
  include_once ABSPATH."/view/404.php";
  exit(0);
}

$instituicaoDao = new InstituicaoDao();
$instituicaoModel = $instituicaoDao->getById($_GET['id'])[0];
$instituicao = $instituicaoModel->toArray();

include_once(ABSPATH.'/dao/enquadramento_dao.php');
$enquadramentoDao = new EnquadramentoDao();
$enquadramentos = array();

foreach ($enquadramentoDao->listar() as $e) {
  $enquadramentos[$e->getIdEnquadramento()] = $e->getEnquadramentoNome();
}

include_once(ABSPATH.'/view/instituicao_view.php');
$view = new InstituicaoView();
$view->consulta($instituicao, $enquadramentos);
$instituicoes[$_GET['id']] = $instituicao;

// lista as unidades da instituição
include_once(ABSPATH.'/dao/unidade_dao.php');
$unidadeDao = new UnidadeDao();
$unidades = array();
foreach ($unidadeDao->getByIdInstituicao($_GET['id']) as $u) {
  $unidades[] = $u->toArray();
}
if (!empty($unidades)) {
  include_once(ABSPATH.'/view/unidade_view.php');
  $viewUnidade = new UnidadeView();
  $viewUnidade->lista($unidades, $instituicoes);
}
