<?php
//carrega unidades
include_once(ABSPATH.'/dao/unidade_dao.php');
include_once(ABSPATH.'/dao/instituicao_dao.php');

$unidadeDao = new unidadeDao();
$instituicaoDao = new instituicaoDao();

//se os dados obrigatorios foram passados
if (isset($_GET['id'])) {
  //remove o unidade do banco
  ($unidadeDao->excluir($_GET['id'])) ? new Message(array(0,'unidade removida com sucesso.')) : new Message(array(1,'Não foi possível remover a unidade.'));
} else {
  new Message( array(3,'É necessário passar o identificador da unidade para excluir.') );
}

$unidades = array();

foreach($unidadeDao->listar() as $u) {
  $unidades[] = $u->toArray();
}

$instituicoes = array();

foreach($instituicaoDao->listar() as $i) {
  $instituicoes[$i->getIdInstituicao()] = $i->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/unidade_view.php');
$view = new unidadeView();
$view->lista($unidades, $instituicoes);
