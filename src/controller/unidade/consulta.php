<?php
//carrega unidades
include_once(ABSPATH.'/dao/unidade_dao.php');

if (!isset($_GET['id'])) {
  //new Message(2, "É necessário informar o id para está requisição");
  include_once ABSPATH."/view/404.php";
  exit(0);
}

$unidadeDao = new UnidadeDao();
$unidadeModel = $unidadeDao->getById($_GET['id'])[0];
$unidade = $unidadeModel->toArray();

include_once(ABSPATH.'/dao/instituicao_dao.php');
$instituicaoDao = new InstituicaoDao();
$instituicoes = array();

foreach ($instituicaoDao->listar() as $i) {
  $instituicoes[$i->getIdInstituicao()] = $i->getInstituicaoNome();
}

include_once(ABSPATH.'/dao/contato_unidade_dao.php');
$contatoUnidadeDao = new ContatoUnidadeDao();
$contatos = array();

foreach ($contatoUnidadeDao->getByIdUnidade($_GET['id']) as $c) {
  $contatos[$c->getIdContatoUnidade()] = $c->toArray();
}

//print_r($contatos);

include_once(ABSPATH.'/view/unidade_view.php');
$view = new UnidadeView();
$view->consulta($unidade, $instituicoes);

include_once(ABSPATH.'/view/contato_unidade_view.php');
$contatoView = new ContatoUnidadeView();
$contatoView->lista($contatos, $unidadeModel->getIdUnidade());
