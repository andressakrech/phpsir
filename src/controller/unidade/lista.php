<?php
include_once(ABSPATH.'/dao/unidade_dao.php');
include_once(ABSPATH.'/dao/instituicao_dao.php');

$unidadeDao = new UnidadeDao();
$instituicaoDao = new InstituicaoDao();

// lista as unidades para carregar na view
$unidades = array();
foreach ($unidadeDao->listar() as $u) {
  $unidades[] = $u->toArray();
}

// lista as instituicoes para carregar na view
$instituicoes = array();
foreach ($instituicaoDao->listar() as $i) {
  $instituicoes[$i->getIdInstituicao()] = $i->toArray();
}


// inclui a view para exibir os dados
include_once(ABSPATH.'/view/unidade_view.php');
$view = new UnidadeView();
$view->lista($unidades, $instituicoes);
