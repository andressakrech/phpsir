<?php
//carrega unidades
include_once(ABSPATH.'/dao/unidade_dao.php');
include_once(ABSPATH.'/dao/instituicao_dao.php');
include_once(ABSPATH.'/dao/endereco_dao.php');

$unidadeDao = new UnidadeDao();
$enderecoModel = new EnderecoModel();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if ( isset($_POST['unidade_nome']) && isset($_POST['unidade_sigla']) && isset($_POST['id_instituicao']) &&
  isset($_POST['endereco']) && isset($_POST['cidade']) && isset($_POST['coordenada']) ) {
    $unidadeModel = new UnidadeModel();
    //adiciona os campos
    $unidadeModel->setUnidadeNome($_POST['unidade_nome']);
    $unidadeModel->setUnidadeSigla($_POST['unidade_sigla']);
    $unidadeModel->setDataConexao($_POST['data_conexao']);
    $unidadeModel->setObservacao($_POST['observacao']);
    $unidadeModel->setIdInstituicao($_POST['id_instituicao']);
    $enderecoModel->setEndereco($_POST['endereco']);
    $enderecoModel->setBairro($_POST['bairro']);
    $enderecoModel->setCidade($_POST['cidade']);
    $enderecoModel->setCep($_POST['cep']);
    $enderecoModel->setCoordenada($_POST['coordenada']);
    $unidadeModel->setEndereco($enderecoModel);

    //insere o unidade no banco
    ($unidadeDao->inserir($unidadeModel) !== null) ? new Message(array(0,'Unidade cadastrada com sucesso.')) : new Message(array(1,'Ocorreu um erro ao cadastrar a unidade.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

include_once(ABSPATH.'/dao/instituicao_dao.php');
$instituicaoDao = new InstituicaoDao();
$instituicoes = array();

foreach ($instituicaoDao->listar() as $e) {
  $instituicoes[$e->getIdInstituicao()] = $e->toArray();
}


// inclui a view para exibir os dados
include_once(ABSPATH.'/view/unidade_view.php');
$view = new UnidadeView();
$view->cadastra($instituicoes);

