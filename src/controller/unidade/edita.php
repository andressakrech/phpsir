<?php
//carrega unidades
include_once(ABSPATH.'/dao/unidade_dao.php');
include_once(ABSPATH.'/dao/instituicao_dao.php');
//include_once(ABSPATH.'/dao/endereco_dao.php');

$unidadeDao = new UnidadeDao();
$enderecoModel = new EnderecoModel();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if ( isset($_POST['unidade_nome']) && isset($_POST['unidade_sigla']) && isset($_POST['id_instituicao']) &&
  isset($_POST['endereco']) && isset($_POST['cidade']) && isset($_POST['coordenada']) && isset($_GET['id'])) {
    $unidadeModel = new UnidadeModel();
    //adiciona os campos
    $unidadeModel->setIdUnidade($_GET['id']);
    $unidadeModel->setUnidadeNome($_POST['unidade_nome']);
    $unidadeModel->setUnidadeSigla($_POST['unidade_sigla']);
    $unidadeModel->setDataConexao($_POST['data_conexao']);
    $unidadeModel->setObservacao($_POST['observacao']);
    $unidadeModel->setIdInstituicao($_POST['id_instituicao']);
    $enderecoModel->setIdEndereco($_POST['id_endereco']);//type hidden no form
    $enderecoModel->setEndereco($_POST['endereco']);
    $enderecoModel->setBairro($_POST['bairro']);
    $enderecoModel->setCidade($_POST['cidade']);
    $enderecoModel->setCep($_POST['cep']);
    $enderecoModel->setCoordenada($_POST['coordenada']);
    $unidadeModel->setEndereco($enderecoModel);

    //insere o unidade no banco
    ($unidadeDao->atualizar($unidadeModel)) ? new Message(array(0,'Unidade atualizada com sucesso.')) : new Message(array(1,'Ocorreu um erro ao atualizar a unidade.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

$instituicaoDao = new InstituicaoDao();
$instituicoes = array();

foreach ($instituicaoDao->listar() as $i) {
  $instituicoes[$i->getIdInstituicao()] = $i->toArray();
}

// verifica se o id da unidade existe senao encaminha para pagina de not found
if ($unidadeDao->getById($_GET['id']) == null) {
  //new Message(1, "O id especificado nao existe.");
  include_once(ABSPATH.'/view/404.php');
  exit(0);
}

$unidade = ($unidadeDao->getById($_GET['id'])[0])->toArray();

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/unidade_view.php');
$view = new UnidadeView();
$view->edita($unidade, $instituicoes);
