<?php
//carrega enquadramentos
//include_once(ABSPATH.'/dao/operadora_dao.php');
include_once(ABSPATH.'/dao/contato_operadora_dao.php');

//$operadoraDao = new OperadoraDao();
$contatoDao = new ContatoOperadoraDao();

$contatoModel = new ContatoOperadoraModel();

include_once(ABSPATH.'/view/contato_operadora_view.php');
$view = new ContatoOperadoraView();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados. obs: Deve ser passado ao menos 1 email ou telefone
  if (isset($_POST['nome']) && isset($_GET['operadora']) && ($_POST['telefone'] || $_POST['email'])) {

    $contatoModel->setNome($_POST['nome']);
    $contatoModel->setObservacao($_POST['observacao']);
    $contatoModel->setEmail($_POST['email']);
    $contatoModel->setTelefone($_POST['telefone']);
    $contatoModel->setIdOperadora($_GET['operadora']);
    
    //insere contato no banco
    ($contatoDao->inserir($contatoModel)) ? new Message(array(0,'Contato cadastrado com sucesso.')) : new Message(array(1,'Ocorreu um erro ao cadastrar o contato.'));

  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

// inclui a view para exibir os dados
$view->cadastra();

