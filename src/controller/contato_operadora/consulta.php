<?php
//carrega contatoOperadoras
include_once(ABSPATH.'/dao/contato_operadora_dao.php');

if (isset($_GET['id'])) {
  $contatoOperadoraDao = new ContatoOperadoraDao();
  $contatoOperadoraModel = $contatoOperadoraDao->getById($_GET['id'])[0];
  $contatoOperadora = $contatoOperadoraModel->toArray();

  include_once(ABSPATH.'/view/contato_operadora_view.php');
  $view = new ContatoOperadoraView();
  $view->consulta($contatoOperadora);
} else {
  //new Message(2, "É necessário informar o id para está requisição");
  include_once ABSPATH."/view/404.php";
}
