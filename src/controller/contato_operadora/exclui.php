<?php
//carrega contato_operadoras
include_once(ABSPATH.'/dao/contato_operadora_dao.php');

$contatoOperadoraDao = new ContatoOperadoraDao();

$contatoModel = $contatoOperadoraDao->getById($_GET['id'])[0];

$idOperadora = $contatoModel->getIdOperadora();

//se os dados obrigatorios foram passados
if (isset($_GET['id'])) {
  //remove o contato_operadora do banco
  ($contatoOperadoraDao->excluir($_GET['id']) !== null) ? new Message(array(0,'Contato removido com sucesso.')) : new Message(array(1,'Não foi possível remover o contato.'));
} else {
  new Message( array(3,'É necessário informar o identificador do contato para excluir.') );
}

include_once(ABSPATH.'/dao/operadora_dao.php');
$operadoraDao = new OperadoraDao();
$operadoraModel = $operadoraDao->getById($idOperadora)[0];
$operadora = $operadoraModel->toArray();

include_once(ABSPATH.'/dao/contato_operadora_dao.php');
$contatoOperadoraDao = new ContatoOperadoraDao();
$contatos = array();

foreach ($contatoOperadoraDao->getByIdOperadora($idOperadora) as $c) {
  $contatos[$c->getIdContatoOperadora()] = $c->toArray();
}

include_once(ABSPATH.'/view/operadora_view.php');
$view = new OperadoraView();
$view->consulta($operadora, $contatos);

