<?php
//carrega conexoes
include_once(ABSPATH.'/dao/conexao_dao.php');

$conexaoDao = new ConexaoDao();

//se os dados obrigatorios foram passados
if (isset($_GET['id'])) {
  //remove o conexao do banco
  ($conexaoDao->excluir($_GET['id']) !== null) ? new Message(array(0,'Conexão removida com sucesso.')) : new Message(array(1,'Não foi possível remover  conexão.'));
} else {
  new Message( array(3,'É necessário informar o identificador da conexão para excluir.') );
}

$conexoes = array();

foreach($conexaoDao->listar() as $e) {
  $conexoes[] = $e->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/conexao_view.php');
$view = new ConexaoView();
$view->lista($conexoes);

