<?php
//carrega conexoes
include_once(ABSPATH.'/dao/conexao_dao.php');

if (isset($_GET['id'])) {
  $conexaoDao = new ConexaoDao();
  $conexaoModel = $conexaoDao->getById($_GET['id'])[0];
  $conexao = $conexaoModel->toArray();

  include_once(ABSPATH.'/view/conexao_view.php');
  $view = new ConexaoView();
  $view->consulta($conexao);
} else {
  //new Message(2, "É necessário informar o id para está requisição");
  include_once ABSPATH."/view/404.php";
}
