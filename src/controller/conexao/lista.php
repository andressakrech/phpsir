<?php
//carrega conexoes
include_once(ABSPATH.'/dao/conexao_dao.php');
$message = array();

$conexaoDao = new ConexaoDao();

$conexoes = array();

foreach($conexaoDao->listar() as $e) {
  $conexoes[] = $e->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/conexao_view.php');
$view = new ConexaoView();
$view->lista($conexoes);

