<?php
//carrega conexoes
include_once(ABSPATH.'/dao/conexao_dao.php');

$conexaoDao = new ConexaoDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if (isset($_POST['conexao']) && isset($_GET['id'])) {
    $conexaoModel = new ConexaoModel();
    //adiciona os campos
    $conexaoModel->setIdConexao($_GET['id']);
    $conexaoModel->setConexao($_POST['conexao']);

    //insere o conexao no banco
    ($conexaoDao->atualizar($conexaoModel) !== null) ? new Message(array(0,'Conexão atualizada com sucesso.')) : new Message(array(1,'Ocorreu um erro ao atualizar a conexão.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}
//se a página for edição carrega as informações do db, senão passa um array vazio para preenchimento
$conexao = $conexaoDao->getById($_GET['id'])[0]->toArray();

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/conexao_view.php');
$view = new ConexaoView();
$view->edita($conexao);

