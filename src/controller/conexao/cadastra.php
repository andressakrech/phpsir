<?php
//carrega conexoes
include_once(ABSPATH.'/dao/conexao_dao.php');
$message = array();

$conexaoDao = new ConexaoDao();
$conexoes = array();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if (isset($_POST['conexao'])) {
    $conexaoModel = new ConexaoModel();
    //adiciona os campos
    $conexaoModel->setConexao($_POST['conexao']);

    //insere o conexao no banco
    ($conexaoDao->inserir($conexaoModel) !== null) ? new Message(array(0,'Conexao cadastrado com sucesso.')) : new Message(array(1,'Ocorreu um erro ao cadastrar a conexao.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/conexao_view.php');
$view = new ConexaoView();
$view->cadastra();

