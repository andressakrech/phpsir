<?php
//carrega unidades
include_once(ABSPATH.'/dao/circuito_dao.php');
include_once(ABSPATH.'/dao/unidade_dao.php');
include_once(ABSPATH.'/dao/operadora_dao.php');
include_once(ABSPATH.'/dao/conexao_dao.php');

$circuitoDao = new CircuitoDao();
$circuitoModel = $circuitoDao->getById($_GET['id'])[0];
$circuito = $circuitoModel->toArray();


include_once(ABSPATH.'/dao/conexao_dao.php');
$conexaoDao = new ConexaoDao();
$conexoes = array();

foreach ($conexaoDao->listar() as $i) {
  $conexoes[$i->getIdConexao()] = $i->toArray();
}

include_once(ABSPATH.'/dao/unidade_dao.php');
$unidadeDao = new UnidadeDao();
$unidades = array();

foreach ($unidadeDao->listar() as $i) {
  $unidades[$i->getIdUnidade()] = $i->toArray();
}

include_once(ABSPATH.'/dao/operadora_dao.php');
$operadoraDao = new OperadoraDao();
$operadoras = array();

foreach ($operadoraDao->listar() as $i) {
  $operadoras[$i->getIdOperadora()] = $i->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/circuito_view.php');
$view = new CircuitoView();
$view->consulta($circuito, $operadoras, $unidades, $conexoes);

