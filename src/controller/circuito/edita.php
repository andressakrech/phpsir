<?php
//carrega unidades
include_once(ABSPATH.'/dao/circuito_dao.php');
include_once(ABSPATH.'/dao/unidade_dao.php');
include_once(ABSPATH.'/dao/operadora_dao.php');
include_once(ABSPATH.'/dao/conexao_dao.php');

$circuitoDao = new CircuitoDao();
$conexaoDao = new ConexaoDao();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if ( isset($_POST['id_operadora']) && isset($_POST['id_unidade']) && isset($_POST['designacao']) && isset($_POST['conexao']) && $_POST['id_circuito'] ) {
    $circuitoModel = new CircuitoModel();
    //adiciona os campos
    $circuitoModel->setIdCircuito($_POST['id_circuito']);
    $circuitoModel->setIdOperadora($_POST['id_operadora']);
    $circuitoModel->setIdUnidade($_POST['id_unidade']);
    $circuitoModel->setDesignacao($_POST['designacao']);
    $circuitoModel->setBanda($_POST['banda']);
    $circuitoModel->setDataAtivacao($_POST['data_ativacao']);
    $circuitoModel->setValorMensal((double)$_POST['valor_mensal']);
    $circuitoModel->setAtivo($_POST['ativo']);
    $circuitoModel->setIdConexao($_POST['conexao']);
    
    //insere o unidade no banco
    ($circuitoDao->atualizar($circuitoModel) !== null) ? new Message(array(0,'Circuito atualizado com sucesso.')) : new Message(array(1,'Ocorreu um erro ao atualizar o circuito.'));
  } else {
    new Message( array(3,'Por favor preencher todos os dados obrigatórios.') );
  }
}

$circuito = $circuitoDao->getById($_GET['id'])[0]->toArray();

include_once(ABSPATH.'/dao/conexao_dao.php');
$conexaoDao = new ConexaoDao();
$conexoes = array();

foreach ($conexaoDao->listar() as $i) {
  $conexoes[$i->getIdConexao()] = $i->toArray();
}

include_once(ABSPATH.'/dao/unidade_dao.php');
$unidadeDao = new UnidadeDao();
$unidades = array();

foreach ($unidadeDao->listar() as $i) {
  $unidades[$i->getIdUnidade()] = $i->toArray();
}

include_once(ABSPATH.'/dao/operadora_dao.php');
$operadoraDao = new OperadoraDao();
$operadoras = array();

foreach ($operadoraDao->listar() as $i) {
  $operadoras[$i->getIdOperadora()] = $i->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/circuito_view.php');
$view = new CircuitoView();
$view->edita($circuito, $operadoras, $unidades, $conexoes);

