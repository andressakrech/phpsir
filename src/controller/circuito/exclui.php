<?php
//carrega unidades
include_once(ABSPATH.'/dao/circuito_dao.php');


$circuitoDao = new CircuitoDao();

// se o id do circuito foi passado
if (isset($_GET['id'])) {
  //tenta excluir a unidade
  ($circuitoDao->excluir($_GET['id'])) ? new Message(array(0, "Circuito removido com sucesso.")) : new Message(array(1, "Ocorreu um erro ao excluir o circuito."));
} else {
  new Message(array(3, "O circuito informado não existe."));
}

$circuitos = array();

foreach ($circuitoDao->listar() as $c) {
  $circuitos[] = $c->toArray();
}

include_once(ABSPATH.'/dao/conexao_dao.php');
$conexaoDao = new ConexaoDao();
$conexoes = array();

foreach ($conexaoDao->listar() as $i) {
  $conexoes[$i->getIdConexao()] = $i->toArray();
}

include_once(ABSPATH.'/dao/unidade_dao.php');
$unidadeDao = new UnidadeDao();
$unidades = array();

foreach ($unidadeDao->listar() as $i) {
  $unidades[$i->getIdUnidade()] = $i->toArray();
}

include_once(ABSPATH.'/dao/operadora_dao.php');
$operadoraDao = new OperadoraDao();
$operadoras = array();

foreach ($operadoraDao->listar() as $i) {
  $operadoras[$i->getIdOperadora()] = $i->toArray();
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/circuito_view.php');
$view = new CircuitoView();
$view->lista($circuitos, $operadoras, $unidades, $conexoes);

