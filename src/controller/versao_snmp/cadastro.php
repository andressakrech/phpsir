<?php
//carrega conexoes
include_once(ABSPATH.'/dao/versao_snmp_dao.php');

$versaoSnmpDao = new VersaoSnmpDao();
$message = array();

//Se os dados foram enviados realiza o cadastro dos dados
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  //se os dados obrigatorios foram passados
  if (isset($_POST['versao_snmp'])) {
    $versaoSnmpModel = new VersaoSnmpModel();
    //adiciona os campos
    $versaoSnmpModel->setVersaoSnmp($_POST['versao_snmp']);

    //insere a versao snmp no banco
    $message[] = ($versaoSnmpDao->inserir($versaoSnmpModel) !== null) ? array('success', 'Versão cadastrada com sucesso.') : array('danger', 'Ocorreu um erro ao cadastrar a versão.');
  } else {
    $message[] = array('info','Por favor preencher todos os dados obrigatórios.');
  }
}

// inclui a view para exibir os dados
include_once(ABSPATH.'/view/versao_snmp/cadastro_view.php');

