<?php
// Evita que usuarios acessem este arquivo diretamente
if ( ! defined('ABSPATH')) exit;

// Inicia a sessão
session_start();

// Verifica o modo para debugar
if ( ! defined('DEBUG') || DEBUG === false ) {
  // Esconde todos os erros
  error_reporting(0);
  ini_set("display_errors", 0);

} else {
  // Mostra todos os erros
  error_reporting(E_ALL);
  ini_set("display_errors", 1);

}


//#TODO rotas para a aplicação
// adiciona o cabecalho da aplicacao
include_once ABSPATH . "/view/header.php";

//se a pagina nao tiver sido definida define a pagina como home
$page = $_GET['page'] ?? 'home';//php7

if ($page=='home') {
  include_once ABSPATH."/view/home.php";
} else {
  // verifica o view, se nao for passado define como lista
  $view = $_GET['view'] ?? 'lista';
  // define o arquivo que contem a view
  $controller = ABSPATH."/controller/{$page}/{$view}.php";
  if (!file_exists($controller)) {
    // se o arquivo não for localizado inclui a pagina de not found
    include_once ABSPATH."/view/404.php";
  } else {
    // inclui a view
    include_once $controller;
  }
}

include_once ABSPATH . "/view/footer.php";
